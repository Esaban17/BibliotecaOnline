import { RouterModule, Routes } from '@angular/router' ;
import { UsuarioComponent } from './components/usuario/usuario.component';
import { AutorComponent } from './components/autor/autor.component';
import { CategoriaComponent } from './components/categoria/categoria.component';
import { EditorialComponent } from './components/editorial/editorial.component';
import { EstadoComponent } from './components/estado/estado.component';
import { PrestamoComponent } from './components/prestamo/prestamo.component';
import { LoginGuard } from './guards/login.guard';
import { LoginComponent } from './components/login/login.component';
import { RegistrarComponent } from './components/registrar/registrar.component';
import { ChatComponent } from './components/chat/chat.component';
import { LibroComponent } from './components/libro/libro.component';
import { PerfilComponent } from './components/perfil/perfil.component';
import { PublicacionComponent } from './components/publicacion/publicacion.component';
import { HomeComponent } from './components/home/home.component';
import { InformacionComponent } from './components/informacion/informacion.component';
import { CalendarioComponent } from './components/calendario/calendario.component';
import { PdfComponent } from './components/pdf/pdf.component';

const APP_ROUTES: Routes = [
    { path: 'login', component: LoginComponent},
    { path: 'registrarse', component: RegistrarComponent},
    { path: 'libros', component: LibroComponent, canActivate: [LoginGuard]},
    { path: 'calendario', component: CalendarioComponent, canActivate: [LoginGuard]},
    { path: 'prestamos', component: PrestamoComponent, canActivate: [LoginGuard]},
    { path: 'publicaciones', component: PublicacionComponent, canActivate: [LoginGuard]},
    { path: 'home', component: HomeComponent},
    { path: 'chat', component: ChatComponent, canActivate: [LoginGuard]},
    { path: 'perfil', component: PerfilComponent, canActivate: [LoginGuard]},
    { path: 'usuarios', component: AutorComponent, canActivate: [LoginGuard]},
    { path: 'autores', component: AutorComponent, canActivate: [LoginGuard]},
    { path: 'categorias', component: CategoriaComponent, canActivate: [LoginGuard]},
    { path: 'editoriales', component: EditorialComponent, canActivate: [LoginGuard]},
    { path: 'estados', component: EstadoComponent, canActivate: [LoginGuard]},
    { path: 'estados', component: EstadoComponent, canActivate: [LoginGuard]},
    { path: 'informacion', component: InformacionComponent, canActivate: [LoginGuard]},
    { path: 'leer/:pdf', component: PdfComponent, canActivate: [LoginGuard]},
    { path: '**', pathMatch: 'full', redirectTo: 'login'}
];

export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES);