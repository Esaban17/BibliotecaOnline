import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { PdfViewerModule } from 'ng2-pdf-viewer';

import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { NgSemanticModule } from 'ng-semantic';
import { HttpModule } from '@angular/http';
import { AppComponent } from './app.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { UsuarioComponent } from './components/usuario/usuario.component';
import { FooterComponent } from './components/footer/footer.component';
import { APP_ROUTING } from './app.routes';
import { UsuarioService } from './services/usuario.service';
import { RolService } from './services/rol.service';
import { AutorService } from './services/autor.service';
import { CategoriaService } from './services/categoria.service';
import { ComentarioService } from './services/comentario.service';
import { EditorialService } from './services/editorial.service';
import { EstadoService } from './services/estado.service';
import { LibroService } from './services/libro.service';
import { MensajeService } from './services/mensaje.service';
import { PaisService } from './services/pais.service';
import { PrestamoService } from './services/prestamo.service';
import { AutorComponent } from './components/autor/autor.component';
import { CategoriaComponent } from './components/categoria/categoria.component';
import { EditorialComponent } from './components/editorial/editorial.component';
import { EstadoComponent } from './components/estado/estado.component';
import { LibroComponent } from './components/libro/libro.component';
import { PaisComponent } from './components/pais/pais.component';
import { PrestamoComponent } from './components/prestamo/prestamo.component';
import { LoginComponent } from './components/login/login.component';
import { RegistrarComponent } from './components/registrar/registrar.component';
import { ChatComponent } from './components/chat/chat.component';
import { LoginGuard } from './guards/login.guard';
import { PerfilComponent } from './components/perfil/perfil.component';
import { PublicacionComponent } from './components/publicacion/publicacion.component';
import { HomeComponent } from './components/home/home.component';
import { InformacionComponent } from './components/informacion/informacion.component';
import { CalendarioComponent } from './components/calendario/calendario.component';
import { PdfComponent } from './components/pdf/pdf.component';


@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    UsuarioComponent,
    FooterComponent,
    AutorComponent,
    CategoriaComponent,
    EditorialComponent,
    EstadoComponent,
    LibroComponent,
    PaisComponent,
    PrestamoComponent,
    LoginComponent,
    RegistrarComponent,
    ChatComponent,
    PerfilComponent,
    PublicacionComponent,
    HomeComponent,
    InformacionComponent,
    CalendarioComponent,
    PdfComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    NgbModule.forRoot(),
    FormsModule,
    APP_ROUTING,
    NgSemanticModule,
    PdfViewerModule
  ],
  providers: [
    UsuarioService,
    RolService,
    AutorService,
    CategoriaService,
    ComentarioService,
    EditorialService,
    EstadoService,
    LibroService,
    MensajeService,
    PaisService,
    PrestamoService,
    LoginGuard
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
