import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { UsuarioService } from '../../services/usuario.service';

@Component({
  selector: 'app-registrar',
  templateUrl: './registrar.component.html',
  styleUrls: ['./registrar.component.css']
})
export class RegistrarComponent implements OnInit {

  nombre: string;
  apellido: string;
  correo: string;
  telefono: string;
  usuario: string;
  pass: string;
  idRol: number;
  msgError: boolean;

  constructor(private usuarioService: UsuarioService, private router: Router) { }

  ngOnInit() {
  }

  registrarse( nombre: string, apellido: string, correo: string, telefono: string, usuario: string,pass: string) {
    if(nombre !='' && apellido != '' && telefono != '' && usuario !='' && pass != ''){
      console.log('Datos Completos');
      const USUARIO = {
        nombre: nombre,
        apellido: apellido,
        correo: correo,
        telefono: telefono,
        usuario: usuario,
        pass: pass,
        idRol: 2
      };
      this.usuarioService.createUsuario(USUARIO).subscribe(
        (datos) => {
        }
      );
      this.msgError = true;
      this.limpiar();
    }else{
      console.log('Datos Incompletos');
      this.msgError = false;
    }
  }

  limpiar(){
    this.nombre = '';
    this.apellido = '';
    this.correo = '';
    this.telefono = '';
    this.usuario = '';
    this.pass = '';
  }
}
