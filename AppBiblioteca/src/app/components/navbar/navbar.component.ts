import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UsuarioService } from '../../services/usuario.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  idRol: number;
  public token;
  usuario: string;
  nombre: string;

  constructor(private router: Router, private usuarioService: UsuarioService) {
    this.token = this.usuarioService.getToken();
  }

  ngDoCheck(){
    this.idRol = parseInt(localStorage.getItem('idRol'));
    this.token = this.usuarioService.getToken();
    this.nombre = localStorage.getItem('nombre');
  }

  ngOnInit() {
    this.idRol = parseInt(localStorage.getItem('idRol'));
    this.token = this.usuarioService.getToken();
  }

  logOut(){
    localStorage.clear();
    this.token = null;
    this.router.navigate(['/home']);
  }

}