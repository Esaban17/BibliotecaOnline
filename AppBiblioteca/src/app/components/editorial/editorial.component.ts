import { Component, OnInit } from '@angular/core';
import { EditorialService } from '../../services/editorial.service';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';

@Component({
  selector: 'app-editorial',
  templateUrl: './editorial.component.html',
  styleUrls: ['./editorial.component.css']
})
export class EditorialComponent implements OnInit {

  idRol: number;
  closeResult: string;
  lista = [];
  listaFija = [];
  msgError = false;
  idEditorial: number;
  editorial: string;

  constructor(private editorialService: EditorialService,private modalService: NgbModal, private router: Router) { }

  ngOnInit() {
    this.idRol = parseInt(localStorage.getItem('idRol'));
    this.getEditoriales();
  }

  //GET
  getEditoriales() {
    this.editorialService.getEditoriales().subscribe(res => {
      this.lista = res.json();
      this.listaFija = res.json();
    });
  }

  //CREATE
  createEditorial( editorial: string) {
    const editorialData = {
      editorial: editorial
    };

    this.editorialService.createEditorial(editorialData).subscribe(
      (datos) => {
        this.getEditoriales();
      }
    );
  }

  //UPDATE
  updateEditorial() {
    const editorialData = {
      idEditorial: this.idEditorial,
      editorial: this.editorial
    };

    this.editorialService.updateEditorial(this.idEditorial,editorialData).subscribe(
      (datos) => {
        this.getEditoriales();
      }
    );
  }

  //DELETE
  deleteEditorial() {
    this.editorialService.deleteEditorial(this.idEditorial).subscribe(
      (data) => {
        this.getEditoriales();
      }
    );
  }

  //SET
  setData(item: any) {
    this.idEditorial = item.idEditorial;
    this.editorial = item.editorial;
  }

  //LIMPIAR
  deleteData(item: any) {
    this.editorial = '';
  }

  //BUSCADOR
  buscarEditorial(text: any) {
    this.lista = this.listaFija;
    const list: any = [];
    if (text !== '') {
      for (const elemento of this.lista){
        if (elemento.editorial.toLowerCase().indexOf(text.toLowerCase()) > -1 ) {
          list.push(elemento);
          this.lista = list;
          this.msgError = false;
        }else {
          if (list.length === 0) {
            this.lista = [];
            this.msgError = true;
          }
        }
      }
    }else {
      this.getEditoriales();
      this.msgError = false;
    }
  }

  open(content) {
    this.modalService.open(content).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }

}

