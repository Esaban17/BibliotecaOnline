import { Component, OnInit } from '@angular/core';
import { UsuarioService } from '../../services/usuario.service';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.component.html',
  styleUrls: ['./perfil.component.css']
})
export class PerfilComponent implements OnInit {
  idUsuario: number;
  nombre: string;
  apellido: string;
  correo: string;
  telefono: string;
  usuario: string;
  password: string;
  foto: string;
  idRol: number;

  constructor(private usuarioService: UsuarioService) { }

  ngOnInit() {
    this.idUsuario = parseInt(localStorage.getItem('idUsuario'));
    this.getUsuario();
  }

  getUsuario(){
    this.usuarioService.getUsuario(this.idUsuario).subscribe(res => {
      this.nombre = res.json()[0].nombre
      this.apellido = res.json()[0].apellido
      this.correo = res.json()[0].correo
      this.telefono = res.json()[0].telefono
      this.usuario = res.json()[0].usuario
      this.password = res.json()[0].pass
      this.foto = res.json()[0].foto
      this.idRol = parseInt(res.json()[0].idRol)
    })
  }

  public subirImagen($event) {
    if ($event.target.files.length === 1) {
        this.usuarioService.subirArchivo($event.target.files[0]).subscribe(response => {
          this.getUsuario();
        },  
        error => {
            console.error(error);
        });
    }
  }

  getImagen(){
    this.usuarioService.getImagen(this.idUsuario).subscribe(res => {
      this.foto = res.json()[0].foto
      return this.foto
    })
  }

  editarPerfil() {
    const IDUSUARIO = this.idUsuario;
    const USUARIO = {
      nombre: this.nombre,
      apellido: this.apellido,
      correo: this.correo,
      telefono: this.telefono,
      usuario: this.usuario,
      pass: this.password,
      foto: this.foto,
      idRol: this.idRol
    };

    this.usuarioService.updateUsuario(USUARIO, IDUSUARIO).subscribe(
      (datos) => {
        console.log('Editado Correctamente')
      }
    );
  }
}
