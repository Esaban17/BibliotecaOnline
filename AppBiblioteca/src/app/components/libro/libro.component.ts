import { Component, OnInit, Inject } from '@angular/core';
import { LibroService } from '../../services/libro.service';
import { AutorService } from '../../services/autor.service';
import { EditorialService } from '../../services/editorial.service';
import { CategoriaService } from '../../services/categoria.service';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';
import { ComentarioService } from '../../services/comentario.service';
import { PrestamoService } from '../../services/prestamo.service';
import * as jsPDF from 'jspdf'

@Component({
  selector: 'app-libro',
  templateUrl: './libro.component.html',
  styleUrls: ['./libro.component.css'],
  providers: [
    { provide: 'Window', useValue: window}
  ]
})

export class LibroComponent implements OnInit {  

  idRol: number;
  idLogueado: number;
  closeResult: string;
  lista = [];
  listaFija = [];
  listaAutores = [];
  listaEditoriales = [];
  listaCategorias = [];
  listaPrestamos = [];
  msgError = false;
  msgError2 = false;
  msgError3 = false;
  msgPrestado = false;
  msgAgotado = false;
  msgCorrecto = false;
  idLibro: number;
  titulo: string;
  idAutor: number;
  idEditorial: number;
  edicion: string;
  tema: string;
  paginas: number;
  descripcion: string;
  idCategoria: number;
  cantidad: number;
  disponibles: number;
  fechaPublicacion: string;
  idEstado: number;
  portada: string;
  idUsuario: number;
  comentario: string;
  idPrestamo: number;

  constructor( @Inject('Window') private window: Window, private prestamoService: PrestamoService,private libroService: LibroService, private autorService: AutorService, private editorialService: EditorialService, private categoriaService: CategoriaService , private modalService: NgbModal, private router: Router) { 
  }
 
  ngOnInit() {
    this.idRol = parseInt(localStorage.getItem('idRol'));
    this.idLogueado = parseInt(localStorage.getItem('idUsuario'));
    this.getAutores();
    this.getEditoriales();
    this.getCategorias();
    this.getLibros();
    this.getPrestamos();
  }

  //ENVIA EL DOCUMENTO AL COMPONENTE PDF
  setPDF(pdf: string) {
    this.router.navigate(['/leer', pdf]);
  }

  //RESTAR DISPONIBLES AL PRESTAR UN LIBRO
  restarDisponibles(idLibro: number){
    const CANTIDAD = {
      idLibro: this.idLibro,
      cantidad: 1
    };
    this.prestamoService.restarDisponibles(CANTIDAD).subscribe(response => {
      this.getPrestamos();
    });
  }
  
  //PRESTAR LIBRO
  prestarLibro() {
    this.msgAgotado =  false;
    this.msgPrestado = false;
    this.msgCorrecto = false
    var agotado = false;
    for(let i = 0; i< this.lista.length; i++){
      if(this.lista[i].idLibro == this.idLibro && this.lista[i].disponibles == 0){
        agotado = true;
      }
    }
    if(agotado){
      const libro = {
        idLibro: this.idLibro,
        titulo: this.titulo,
        idAutor: this.idAutor,
        idEditorial: this.idEditorial,
        edicion: this.edicion,
        tema: this.tema,
        paginas: this.paginas,
        descripcion: this.descripcion,
        idCategoria: this.idCategoria,
        cantidad: this.cantidad,
        disponibles: 0,
        fechaPublicacion: this.fechaPublicacion,
        idEstado: 2
      };
      this.libroService.updateLibro(this.idLibro,libro).subscribe(
        (datos) => {
          this.getLibros();
        }
      );
      console.log('LIBRO AGOTADO');
      this.msgAgotado = true;
    }else{
      var prestar = true;
      for(let i = 0; i< this.listaPrestamos.length; i++){
        if(this.listaPrestamos[i].idLibro == this.idLibro && this.listaPrestamos[i].idUsuario == this.idLogueado){
          prestar = false;
        }else{
          prestar = true;
        }
      }
      if(prestar){
        // Creamos una fecha (en el día actual)
        var fecha = new Date();
        var dia = fecha.getDate();
        var mes = fecha.getMonth() + 1;
        var anio = fecha.getFullYear();
        // Cantidad de dias permitidos para devolverlo
        var diasDevolucion = 15;
        // Creamos la fecha de devolucion
        var fechaDevolucion = new Date();
        // Sumamos los dias a la fecha
        fechaDevolucion.setDate(fechaDevolucion.getDate() + diasDevolucion);
        // Obtenemos el dia, mes y año de la fecha de devolucion
        var diaDevolucion = fechaDevolucion.getDate();
        var mesDevolucion = fechaDevolucion.getMonth() + 1;
        var anioDevolucion = fechaDevolucion.getFullYear();
        var fechaPrestamo = anio + "/" + mes + "/" + dia;
        var fechaEntrega = anioDevolucion + "/" + mesDevolucion + "/" + diaDevolucion;
  
        const prestamo = {
          idLibro: this.idLibro,
          idUsuario: this.idLogueado,
          fechaInicio: fechaPrestamo,
          fechaFinal: fechaEntrega
        };
        this.prestamoService.createPrestamo(prestamo).subscribe(
          (datos) => {
            console.log('Prestamo Realizado Correctamente');
            this.msgCorrecto = true;
          }
        );
        this.restarDisponibles(this.idLibro);
        this.generarTicket();
      }else{
        this.msgPrestado = true;
        console.log("Este Libro Ya Lo Tienes Prestado");
      }     
    }
  }

  //SUBIR PORTADA
  public subirPortada(idLibro: number,$event) {
    if ($event.target.files.length === 1) {
        this.libroService.subirPortada(idLibro, $event.target.files[0]).subscribe(response => {
          this.getLibros();
        },  
        error => {
            console.error(error);
        });
    }
  }

  //OBTENER-AUTOR
  obtenerAutor(idAutor){
    for(let i = 0; i< this.listaAutores.length; i++){
      if(this.listaAutores[i].idAutor === idAutor){
        return this.listaAutores[i].nombre + this.listaAutores[i].apellido;
      }
    }
  }

  //OBTENER-FECHA PRESTAMO
  obtenerFechaPrestamo(){
    // Creamos una fecha (en el día actual)
    var fecha = new Date();
    var dia = fecha.getDate();
    var mes = fecha.getMonth() + 1;
    var anio = fecha.getFullYear();

    var fechaPrestamo = dia + "/" + mes + "/" + anio;

    return fechaPrestamo; 
  }

  //OBTENER-FECHA PRESTAMO
  obtenerFechaEntrega(){
    // Creamos una fecha (en el día actual)
    var fecha = new Date();
    var dia = fecha.getDate();
    var mes = fecha.getMonth() + 1;
    var anio = fecha.getFullYear();
    // Cantidad de dias permitidos para devolverlo
    var diasDevolucion = 15;
    // Creamos la fecha de devolucion
    var fechaDevolucion = new Date();
    // Sumamos los dias a la fecha
    fechaDevolucion.setDate(fechaDevolucion.getDate() + diasDevolucion);
    // Obtenemos el dia, mes y año de la fecha de devolucion
    var diaDevolucion = fechaDevolucion.getDate();
    var mesDevolucion = fechaDevolucion.getMonth() + 1;
    var anioDevolucion = fechaDevolucion.getFullYear();
    var fechaEntrega = diaDevolucion + "/" + mesDevolucion + "/" + anioDevolucion;
    return fechaEntrega;
  }

  //OBTENER-EDITORIAL
  obtenerEditorial(idEditorial){
    for(let i = 0; i< this.listaEditoriales.length; i++){
      if(this.listaEditoriales[i].idEditorial === idEditorial){
        return this.listaEditoriales[i].editorial;
      }
    }
  }

  //OBTENER-CATEGORIA
  obtenerCategoria(idCategoria){
    for(let i = 0; i< this.listaCategorias.length; i++){
      if(this.listaCategorias[i].idCategoria === idCategoria){
        return this.listaCategorias[i].categoria;
      }
    }
  }

  //GET-LIBROS
  getLibros() {
    this.libroService.getLibros().subscribe(res => {
      this.lista = res.json();
      this.listaFija = res.json();
    });
  }

  //GET-PRESTAMOS
  getPrestamos() {
    this.prestamoService.getPrestamos().subscribe(res => {
      this.listaPrestamos = res.json();
    });
  }

  //GET-AUTORES
  getAutores() {
    this.autorService.getAutores().subscribe(res => {
      this.listaAutores = res.json();
    });
  }

  //GET-EDITORIALES
  getEditoriales() {
    this.editorialService.getEditoriales().subscribe(res => {
      this.listaEditoriales = res.json();
    });
  }

  //GET-CATEGORIAS
  getCategorias() {
    this.categoriaService.getCategorias().subscribe(res => {
    this.listaCategorias = res.json();
    });
  }

  //CREAR LIBRO
  createLibro(titulo: string, idAutor: number, idEditorial: number, edicion: string, tema: string, paginas: number, descripcion: string, idCategoria: number, cantidad: number, fechaPublicacion : string) {
    const libro = {
      titulo: titulo,
      idAutor: idAutor,
      idEditorial: idEditorial,
      edicion: edicion,
      tema: tema,
      paginas: paginas,
      descripcion: descripcion,
      idCategoria: idCategoria,
      cantidad: cantidad,
      disponibles: cantidad,
      fechaPublicacion: fechaPublicacion,
      idEstado: 1
    };
    this.libroService.createLibro(libro).subscribe(
      (datos) => {
        this.getLibros();
      }
    );
  }


  //ELIMINAR LIBRO
  deleteLibro() {
    this.libroService.deleteLibro(this.idLibro).subscribe(
      (data) => {
        this.getLibros();
      }
    );
  }

  //VERIFICAMOS LA CANTIDAD A LA HORA DE MODIFICAR
  updateLibro(){
    let cantidadPrestada = 0;
    for(let i = 0; i< this.listaPrestamos.length; i++){
      if(this.listaPrestamos[i].idLibro == this.idLibro){
        cantidadPrestada = cantidadPrestada + this.listaPrestamos[i].cantidad;
      }
    }
    var total = this.cantidad - cantidadPrestada;
    if(total >= 0){
      const libro = {
        idLibro: this.idLibro,
        titulo: this.titulo,
        idAutor: this.idAutor,
        idEditorial: this.idEditorial,
        edicion: this.edicion,
        tema: this.tema,
        paginas: this.paginas,
        descripcion: this.descripcion,
        idCategoria: this.idCategoria,
        cantidad: this.cantidad,
        disponibles: total,
        fechaPublicacion: this.fechaPublicacion,
        idEstado: this.idEstado
      };
      this.libroService.updateLibro(this.idLibro,libro).subscribe(
        (datos) => {
          this.getLibros();
        }
      );
    }else{
      console.log('Verifica La Cantidad Tienes ' + cantidadPrestada + " Libros Prestados");
    }
  }

  //SET
  setData(item: any) {
    this.idLibro = item.idLibro;
    this.titulo = item.titulo;
    this.idAutor = item.idAutor;
    this.idEditorial = item.idEditorial;
    this.edicion = item.edicion;
    this.tema = item.tema;
    this.paginas = item.paginas;
    this.descripcion = item.descripcion;
    this.idCategoria = item.idCategoria;
    this.cantidad = item.cantidad;
    this.disponibles = item.disponibles;
    this.fechaPublicacion = item.fechaPublicacion;
    this.idEstado = item.idEstado;
    this.portada = item.portada
  }

  //LIMPIAR
  deleteData(item: any) {
    this.titulo = '';
    this.edicion = '';
    this.tema = '';
    this.descripcion = '';
    this.fechaPublicacion = '';
    this.portada = '';
  }

  //FILTRAR LIBROS CATEGORIA
  filtrarCategoria(idCategoria: number) {
    console.log(idCategoria);
    this.lista = this.listaFija;
    const list: any = [];
    if (this.idCategoria > 0) {
      for (const elemento of this.lista){
        if (elemento.idCategoria == this.idCategoria) {
          list.push(elemento);
          this.lista = list;
          this.msgError2 = false;
        }else {
          if (list.length === 0) {
            this.lista = [];
            this.msgError2 = true;
          }
        }
      }
    }else {
      this.getLibros();
      this.msgError2 = false;
    }
  }  

  //FILTRAR LIBROS EDITORIAL
  filtrarEditorial(idEditorial: number) {
    console.log(idEditorial);
    this.lista = this.listaFija;
    const list: any = [];
    if (this.idEditorial > 0) {
      for (const elemento of this.lista){
        if (elemento.idEditorial == this.idEditorial) {
          list.push(elemento);
          this.lista = list;
          this.msgError3 = false;
        }else {
          if (list.length === 0) {
            this.lista = [];
            this.msgError3 = true;
          }
        }
      }
    }else {
      this.getLibros();
      this.msgError3 = false;
    }
  } 

  //BUSCADOR
  buscarLibro(text: any) {
    this.lista = this.listaFija;
    const list: any = [];
    if (text !== '') {
      for (const elemento of this.lista){
        if (elemento.titulo.toLowerCase().indexOf(text.toLowerCase()) > -1 ||
          elemento.tema.toLowerCase().indexOf(text.toLowerCase()) > -1) {
          list.push(elemento);
          this.lista = list;
          this.msgError = false;
        }else {
          if (list.length === 0) {
            this.lista = [];
            this.msgError = true;
          }
        }
      }
    }else {
      this.getLibros();
      this.msgError = false;
    }
  }

  generarTicket(){
    var ticket = new jsPDF()
    var fechaPrestamo = this.obtenerFechaPrestamo();
    var fechaEntrega = this.obtenerFechaEntrega();
    var source = window.document.getElementsByTagName('porta')[0];

    ticket.setFontSize(10)
    //Horizontal 1
    ticket.line(40,5,175,5);
    //Vertical 1
    ticket.line(40,5,40,70);
    ticket.setFont('arial');
    ticket.text("TICKET PARA UN LIBRO", 90 , 15)
    ticket.text("Libro: " + this.titulo, 45, 25)
    ticket.text("Fecha Prestamo: " + fechaPrestamo, 45 , 35)
    ticket.text("Fecha Entrega: " + fechaEntrega, 45 , 45)
    ticket.setFontSize(10).text("DEBERAS ENTREGAR EL LIBRO A TIEMPO COSTO POR MORA Q 2.00: ", 45 , 60);
    //Horizontal 2
    ticket.line(40,70,175,70);
    //Vertical 2
    ticket.line(175,5,175,70);
    ticket.setFontSize(15).text("Instrucciones:", 40 , 80);
    ticket.setFontSize(10).text("1-Presenta este ticket y tu carnet en Biblioteca para obtener tu libro :)", 40 , 85);
    ticket.setFontSize(10).text("2-Devuelve el libro y se te devolvera tu carnet.", 40 , 90);
    ticket.save('Ticket ' + this.titulo)
  }

  open(content) {
      this.modalService.open(content).result.then((result) => {
        this.closeResult = `Closed with: ${result}`;
      }, (reason) => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      });
  }

  private getDismissReason(reason: any): string {
      if (reason === ModalDismissReasons.ESC) {
        return 'by pressing ESC';
      } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
        return 'by clicking on a backdrop';
      } else {
        return  `with: ${reason}`;
      }
  }
}

