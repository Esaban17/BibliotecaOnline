import { Component, OnInit } from '@angular/core';
import { AutorService } from '../../services/autor.service';
import { PaisService } from '../../services/pais.service';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';

@Component({
  selector: 'app-autor',
  templateUrl: './autor.component.html',
  styleUrls: ['./autor.component.css']
})
export class AutorComponent implements OnInit {

  idRol: number;
  closeResult: string;
  lista = [];
  listaFija = [];
  listaPaises = [];
  msgError = false;
  idAutor: number;
  nombre: string;
  apellido: string;
  biografia: string;
  idPais: number;

  constructor(private autorService: AutorService,private paisService: PaisService, private modalService: NgbModal, private router: Router) { }

  ngOnInit() {
    this.idRol = parseInt(localStorage.getItem('idRol'));
    this.getPaises();
    this.getAutores();
  }

  //SUBIR FOTO
  public subirFoto(idAutor: number,$event) {
    if ($event.target.files.length === 1) {
        this.autorService.subirFoto(idAutor, $event.target.files[0]).subscribe(response => {
          this.getAutores();
        },  
        error => {
            console.error(error);
        });
    }
  }
  //OBTENER
  obtenerPais(idPais){
    for(let i = 0; i< this.listaPaises.length; i++){
      if(this.listaPaises[i].idPais === idPais){
        return this.listaPaises[i].pais;
      }
    }
  }

  //GET-AUTORES
  getAutores() {
    this.autorService.getAutores().subscribe(res => {
    this.lista = res.json();
    this.listaFija = res.json();
    });
  }

  //GET-PAISES
  getPaises() {
    this.paisService.getPaises().subscribe(res => {
    this.listaPaises = res.json();
    });
  }

  //CREATE
  createAutor( nombre: string, apellido: string,biografia: string, idPais: number) {
    const autor = {
      nombre: nombre,
      apellido: apellido,
      biografia: biografia,
      idPais: idPais
    };

    this.autorService.createAutor(autor).subscribe(
      (datos) => {
        this.getAutores();
      }
    );
  }

  //UPDATE
  updateAutor() {
    const autor = {
      idAutor: this.idAutor,
      nombre: this.nombre,
      apellido: this.apellido,
      biografia: this.biografia,
      idPais: this.idPais
    };

    this.autorService.updateAutor(this.idAutor,autor).subscribe(
      (datos) => {
        this.getAutores();
      }
    );
  }

  //DELETE
  deleteAutor() {
    this.autorService.deleteAutor(this.idAutor).subscribe(
      (data) => {
        this.getAutores();
      }
    );
  }

  //SET
  setData(item: any) {
    this.idAutor = item.idAutor;
    this.nombre = item.nombre;
    this.apellido = item.apellido;
    this.biografia = item.biografia;
    this.idPais = item.idPais;
  }

  //LIMPIAR
  deleteData(item: any) {
    this.nombre = '';
    this.apellido = '';
    this.biografia = '';
  }

  //BUSCADOR
  buscarAutor(text: any) {
      this.lista = this.listaFija;
      const list: any = [];
      if (text !== '') {
        for (const elemento of this.lista){
          if (elemento.nombre.toLowerCase().indexOf(text.toLowerCase()) > -1 ||
            elemento.apellido.toLowerCase().indexOf(text.toLowerCase()) > -1) {
            list.push(elemento);
            this.lista = list;
            this.msgError = false;
          }else {
            if (list.length === 0) {
              this.lista = [];
              this.msgError = true;
            }
          }
        }
      }else {
        this.getAutores();
        this.msgError = false;
      }
  }
  
  open(content) {
      this.modalService.open(content).result.then((result) => {
        this.closeResult = `Closed with: ${result}`;
      }, (reason) => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      });
  }
  
  private getDismissReason(reason: any): string {
      if (reason === ModalDismissReasons.ESC) {
        return 'by pressing ESC';
      } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
        return 'by clicking on a backdrop';
      } else {
        return  `with: ${reason}`;
      }
  }

}