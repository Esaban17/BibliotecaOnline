import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './pdf.component.html',
  styleUrls: ['./pdf.component.css']
})
export class PdfComponent implements OnInit {

  page: number = 1;
  pdfSrc: string = '';

  constructor(private activatedRouted: ActivatedRoute) { }

  ngOnInit() {
    try {
      this.activatedRouted.params.subscribe(pdf => {
        this.pdfSrc = 'http://localhost:3000/libro/documento/'+pdf['pdf'];
      });  
    } catch (error) {
      
    }
  } 

  atras(){
    this.page = this.page - 1;
  }

  siguiente(){
    this.page = this.page + 1;
  }
}
