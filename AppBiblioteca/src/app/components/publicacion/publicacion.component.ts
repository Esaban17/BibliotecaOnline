import { Component, OnInit } from '@angular/core';
import { LibroService } from '../../services/libro.service';
import { AutorService } from '../../services/autor.service';
import { EditorialService } from '../../services/editorial.service';
import { CategoriaService } from '../../services/categoria.service';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';
import { ComentarioService } from '../../services/comentario.service';
import { UsuarioService } from '../../services/usuario.service';

@Component({
  selector: 'app-libro',
  templateUrl: './publicacion.component.html',
  styleUrls: ['./publicacion.component.css']
})

export class PublicacionComponent implements OnInit {  

  closeResult: string;
  listaComentarios = [];
  listaComentariosLibro = [];
  lista = [];
  listaAutores = [];
  listaUsuarios = [];
  listaEditoriales = [];
  listaCategorias = [];
  listaFija = [];
  msgError = false;
  idLibro: number;
  titulo: string;
  idAutor: number;
  idEditorial: number;
  edicion: string;
  tema: string;
  paginas: number;
  descripcion: string;
  idCategoria: number;
  cantidad: number;
  disponibles: number;
  año: string;
  portada: string;
  idUsuario: number;
  comentario: string;

  constructor(private usuarioService: UsuarioService,private comentarioService: ComentarioService, private libroService: LibroService, private autorService: AutorService, private editorialService: EditorialService, private categoriaService: CategoriaService , private modalService: NgbModal, private router: Router) { 
  }
 
  ngOnInit() {
    this.getLibros();
    this.getAutores();
    this.getEditoriales();
    this.getCategorias();
    this.getUsuarios();
    this.getComentarios();
  }

  //OBTENER-AUTOR
  obtenerAutor(idAutor){
    for(let i = 0; i< this.listaAutores.length; i++){
      if(this.listaAutores[i].idAutor === idAutor){
      return this.listaAutores[i].autor;
      }
    }
  }

  //OBTENER-EDITORIAL
  obtenerEditorial(idEditorial){
    for(let i = 0; i< this.listaEditoriales.length; i++){
      if(this.listaEditoriales[i].idEditorial === idEditorial){
        return this.listaEditoriales[i].editorial;
      }
    }
  }

  //OBTENER-USUARIOS
  obtenerUsuario(idUsuario){
    for(let i = 0; i< this.listaUsuarios.length; i++){
      if(this.listaUsuarios[i].idUsuario === idUsuario){
        return this.listaUsuarios[i].nombre;
      }
    }
  }

  //OBTENER-FOTO
  obtenerFoto(idUsuario){
    for(let i = 0; i< this.listaUsuarios.length; i++){
      if(this.listaUsuarios[i].idUsuario === idUsuario){
        return this.listaUsuarios[i].foto;
      }
    }
  }

  //OBTENER-CATEGORIA
  obtenerCategoria(idCategoria){
    for(let i = 0; i< this.listaCategorias.length; i++){
      if(this.listaCategorias[i].idCategoria === idCategoria){
        return this.listaCategorias[i].categoria;
      }
    }
  }

  //OBTENER-COMENTARIOS LIBRO
  comentariosLibro(idLibro){
    this.listaComentariosLibro = [];
    for(let i = 0; i< this.listaComentarios.length; i++){
      if(this.listaComentarios[i].idLibro === idLibro){
        this.listaComentariosLibro.push(this.listaComentarios[i]);
      }
    }
  }

  //GET-LIBROS
  getLibros() {
    this.libroService.getLibros().subscribe(res => {
      this.lista = res.json();
      this.listaFija = res.json();
    });
  }

  //GET-USUARIOS
  getUsuarios() {
    this.usuarioService.getUsuarios().subscribe(res => {
      this.listaUsuarios = res.json();
    });
  }

  //GET-AUTORES
  getAutores() {
    this.autorService.getAutores().subscribe(res => {
    this.listaAutores = res.json();
    });
  }

  //GET-EDITORIALES
  getEditoriales() {
    this.editorialService.getEditoriales().subscribe(res => {
    this.listaEditoriales = res.json();
    });
  }

  //GET-CATEGORIAS
  getCategorias() {
    this.categoriaService.getCategorias().subscribe(res => {
    this.listaCategorias = res.json();
    });
  }

  //GET-COMENTARIOS
  getComentarios() {
    this.comentarioService.getComentarios().subscribe(res => {
      this.listaComentarios = res.json();
    });
  }
  
  //CREATE COMENTARIO
  crearComentario(idLibro: number, comentario: string) {
    console.log(idLibro);
    const COMENTARIO = {
      idLibro: idLibro,
      idUsuario: parseInt(localStorage.getItem('idUsuario')),
      comentario: comentario
    };
    this.comentarioService.createComentario(COMENTARIO).subscribe(
      (datos) => {
        this.getComentarios();
      }
    );
  }

  //CREATE
  createLibro(titulo: string, idAutor: number, idEditorial: number, edicion: string, tema: string, paginas: number, descripcion: string, idCategoria: number, cantidad: number, disponibles: number, año: string, portada: string) {
    const libro = {
      titulo: titulo,
      idAutor: idAutor,
      idEditorial: idEditorial,
      edicion: edicion,
      tema: tema,
      paginas: paginas,
      descripcion: descripcion,
      idCategoria: idCategoria,
      cantidad: cantidad,
      disponibles: disponibles,
      año: año,
      portada: portada
    };

    this.libroService.createLibro(libro).subscribe(
      (datos) => {
        this.getLibros();
      }
    );
  }
  
  //UPDATE
  updateLibro() {
    const libro = {
      idLibro: this.idLibro,
      titulo: this.titulo,
      idAutor: this.idAutor,
      idEditorial: this.idEditorial,
      edicion: this.edicion,
      tema: this.tema,
      paginas: this.paginas,
      descripcion: this.descripcion,
      idCategoria: this.idCategoria,
      cantidad: this.cantidad,
      disponibles: this.disponibles,
      año: this.año,
      portada: this.portada
    };

    this.libroService.updateLibro(this.idLibro,libro).subscribe(
      (datos) => {
        this.getLibros();
      }
    );
  }

  //DELETE
  deleteLibro() {
    this.libroService.deleteLibro(this.idLibro).subscribe(
      (data) => {
        this.getLibros();
      }
    );
  }

  //SET
  setData(item: any) {
    this.idLibro = item.idLibro;
    this.titulo = item.titulo;
    this.idAutor = item.idAutor;
    this.idEditorial = item.idEditorial;
    this.edicion = item.edicion;
    this.tema = item.tema;
    this.paginas = item.paginas;
    this.descripcion = item.descripcion;
    this.idCategoria = item.idCategoria;
    this.cantidad = item.cantidad;
    this.disponibles = item.disponibles;
    this.año = item.año;
    this.portada = item.portada
  }

  //LIMPIAR
  deleteData(item: any) {
    this.titulo = '';
    this.edicion = '';
    this.tema = '';
    this.descripcion = '';
    this.año = '';
    this.portada = '';
    this.comentario = '';
  }

  //BUSCADOR
  buscarLibro(text: any) {
    this.lista = this.listaFija;
    const list: any = [];
    if (text !== '') {
      for (const elemento of this.lista){
        if (elemento.titulo.toLowerCase().indexOf(text.toLowerCase()) > -1 ||
          elemento.tema.toLowerCase().indexOf(text.toLowerCase()) > -1) {
          list.push(elemento);
          this.lista = list;
          this.msgError = false;
        }else {
          if (list.length === 0) {
            this.lista = [];
            this.msgError = true;
          }
        }
      }
    }else {
      this.getLibros();
      this.msgError = false;
    }
}

open(content) {
    this.modalService.open(content).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
}

private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
}
}
