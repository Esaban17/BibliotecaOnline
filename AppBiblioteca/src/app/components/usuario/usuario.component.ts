import { Component, OnInit } from '@angular/core';
import { UsuarioService } from '../../services/usuario.service';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { dashCaseToCamelCase } from '@angular/compiler/src/util';
import { RolService } from '../../services/rol.service';

@Component({
  selector: 'app-usuarios',
  templateUrl: './usuario.component.html',
  styleUrls: ['./usuario.component.css']
})
export class UsuarioComponent implements OnInit {

  closeResult: string;
  lista = [];
  listaRoles = [];
  listaFija = [];
  idUsuario: number;
  nombre: string;
  apellido: string;
  telefono: string;
  usuario: string;
  password: string;
  foto: string;
  idRol: number;
  msgError = false;

  constructor(private usuarioService: UsuarioService,private rolService: RolService,private modalService: NgbModal) { }

  ngOnInit() {
    this.getUsuarios();
    this.getRoles();
  }
  
  buscarUsuario(text: any) {
    this.lista = this.listaFija;
    const list: any = [];
    if (text !== '') {
      for (const elemento of this.lista){
        if (elemento.usuario.toLowerCase().indexOf(text.toLowerCase()) > -1 ||
           elemento.nombre.toLowerCase().indexOf(text.toLowerCase()) > -1 || elemento.apellido.toLowerCase().indexOf(text.toLowerCase()) > -1) {
          list.push(elemento);
          this.lista = list;
          this.msgError = false;
        }else {
          if (list.length === 0) {
            this.lista = [];
            this.msgError = true;
          }
        }
      }
    }else {
      this.getUsuarios();
      this.msgError = false;
    }
  }

  getUsuarios() {
    this.usuarioService.getUsuarios().subscribe(res => {
      this.lista = res.json();
      this.listaFija = res.json();
    });
  }

  getRoles() {
    this.rolService.getRoles().subscribe(res => {
      this.listaRoles = res.json();
    });
  }

  createUsuario(nombre: string, apellido: string, telefono: string, usuario: string, password: string, foto: string, idRol: number) {
    console.log(this.idRol);
    const USUARIO = {
      nombre: nombre,
      apellido: apellido,
      telefono: telefono,
      usuario: usuario,
      password: password,
      foto: foto,
      idRol: idRol
    };

    this.usuarioService.createUsuario(USUARIO).subscribe(
      (datos) => {
        this.getUsuarios();
      }
    );

  }

  updateUsuario() {
    const IDUSUARIO = this.idUsuario;
    const USUARIO = {
      nombre: this.nombre,
      apellido: this.apellido,
      telefono: this.telefono,
      usuario: this.usuario,
      password: this.password,
      foto: this.foto,
      idRol: this.idRol
    };

    this.usuarioService.updateUsuario(USUARIO, IDUSUARIO).subscribe(
      (datos) => {
        this.getUsuarios();
      }
    );
  }

  deleteUsuario() {
    this.usuarioService.deleteUsuario(this.idUsuario).subscribe(
      (data) => {
        this.getUsuarios();
      }
    );
  }

  setData(item: any) {
    this.idUsuario = item.idUsuario;
    this.nombre = item.nombre;
    this.apellido = item.apellido;
    this.telefono = item.telefono;
    this.usuario = item.usuario;
    this.password = item.password;
    this.foto = item.foto;
    this.idRol = item.idRol
  }

  deleteData(item: any) {
    this.nombre = '';
    this.apellido = '';
    this.telefono = '';
    this.usuario = '';
    this.password = '';
    this.foto = '';
  }

  open(content) {
    this.modalService.open(content).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }
}