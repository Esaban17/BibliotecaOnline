import { Component, OnInit } from '@angular/core';
import { MensajeService } from '../../services/mensaje.service';
import { Router } from '@angular/router';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { UsuarioService } from '../../services/usuario.service';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})
export class ChatComponent implements OnInit{

  closeResult: string;
  lista = [];
  listaMensajes = [];
  listaUsuarios = [];
  listaFija = [];
  msgError = false;
  idUsuario: number;
  nombre: string;
  apellido: string;
  telefono: string;
  usuario: string;
  foto: string;
  idMensaje: number;
  idEmisor: number;
  idReceptor:number;
  mensaje: string;
  idLogueado: number;
  fotoLogueado: string;
  chatActivo = false;
  idIntervalo: any;

  constructor(private mensajeService: MensajeService,private usuarioService: UsuarioService,private modalService: NgbModal,private router: Router) { }

  ngOnInit() {
    this.idLogueado = parseInt(localStorage.getItem('idUsuario'));
    this.getUsuarios();
    this.getUsuario();
  }

  getMensajes() {
    this.mensajeService.getMensajes(this.idLogueado, this.idUsuario).subscribe(res => {
      this.listaMensajes = res.json();
    });
  }

  getUsuario(){
    console.log(this.idLogueado);
    this.usuarioService.getUsuario(this.idLogueado).subscribe(res => {
      this.fotoLogueado = res.json()[0].foto;
    })
  }

  buscarUsuario(text: any) {
    this.lista = this.listaFija;
    const list: any = [];
    if (text !== '') {
      for (const elemento of this.lista){
        if (elemento.usuario.toLowerCase().indexOf(text.toLowerCase()) > -1 ||
           elemento.nombre.toLowerCase().indexOf(text.toLowerCase()) > -1 || elemento.apellido.toLowerCase().indexOf(text.toLowerCase()) > -1) {
          list.push(elemento);
          this.lista = list;
          this.msgError = false;
        }else {
          if (list.length === 0) {
            this.lista = [];
            this.msgError = true;
          }
        }
      }
    }else {
      this.msgError = false;
    }
  }

  getUsuarios() {
    this.listaUsuarios = [];
    this.usuarioService.getUsuarios().subscribe(res => {
      this.listaUsuarios = res.json();
      for(const usuario of this.listaUsuarios){
        if(usuario.idUsuario != parseInt(localStorage.getItem('idUsuario'))){
          this.lista.push(usuario);
          this.listaFija.push(usuario);
        }
      }
    });
  }

  createMensaje(mensaje: string) {
    const MENSAJE = {
      idEmisor: localStorage.getItem('idUsuario'),
      idReceptor: this.idUsuario,
      mensaje: mensaje
    };
    this.mensajeService.createMensaje(MENSAJE).subscribe(
      (datos) => {
        this.getMensajes();
      }
    );
  }

  setDataUsuario(item: any) {
    this.idUsuario = item.idUsuario;
    this.nombre = item.nombre;
    this.apellido = item.apellido;
    this.telefono = item.telefono;
    this.usuario = item.usuario;
    this.foto = item.foto;
    this.idIntervalo = setInterval(()=> {
      this.getMensajes();
    },1000); 
  }

  //LIMPIAR INTERVALO
  limpiarIntervalo(){
    clearInterval(this.idIntervalo);
  }

  //LIMPIAR
  deleteData(item: any) {
    this.mensaje = '';
  }

  open(content) {
    this.modalService.open(content).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }
}
