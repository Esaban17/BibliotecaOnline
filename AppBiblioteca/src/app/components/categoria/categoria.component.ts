import { Component, OnInit } from '@angular/core';
import { CategoriaService } from '../../services/categoria.service';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';

@Component({
  selector: 'app-categoria',
  templateUrl: './categoria.component.html',
  styleUrls: ['./categoria.component.css']
})
export class CategoriaComponent implements OnInit {

  idRol: number;
  closeResult: string;
  lista = [];
  listaFija = [];
  msgError = false;
  idCategoria: number;
  categoria: string;

  constructor(private categoriaService: CategoriaService,private modalService: NgbModal, private router: Router) { }

  ngOnInit() {
    this.idRol = parseInt(localStorage.getItem('idRol'));
    this.getCategorias();
  }

  //GET
  getCategorias() {
    this.categoriaService.getCategorias().subscribe(res => {
      this.lista = res.json();
      this.listaFija = res.json();
    });
  }

  //CREATE
  createCategoria( categoria: string) {
    const categoriaData = {
      categoria: categoria
    };

    this.categoriaService.createCategoria(categoriaData).subscribe(
      (datos) => {
        this.getCategorias();
      }
    );
  }

  //UPDATE
  updateCategoria() {
    const categoriaData = {
      idCategoria: this.idCategoria,
      categoria: this.categoria
    };

    this.categoriaService.updateCategoria(this.idCategoria,categoriaData).subscribe(
      (datos) => {
        this.getCategorias();
      }
    );
  }

  //DELETE
  deleteCategoria() {
    this.categoriaService.deleteCategoria(this.idCategoria).subscribe(
      (data) => {
        this.getCategorias();
      }
    );
  }

  //SET
  setData(item: any) {
    this.idCategoria = item.idCategoria;
    this.categoria = item.categoria;
  }

  //LIMPIAR
  deleteData(item: any) {
    this.categoria = '';
  }

  //BUSCADOR
  buscarCategoria(text: any) {
    this.lista = this.listaFija;
    const list: any = [];
    if (text !== '') {
      for (const elemento of this.lista){
        if (elemento.categoria.toLowerCase().indexOf(text.toLowerCase()) > -1 ) {
          list.push(elemento);
          this.lista = list;
          this.msgError = false;
        }else {
          if (list.length === 0) {
            this.lista = [];
            this.msgError = true;
          }
        }
      }
    }else {
      this.getCategorias();
      this.msgError = false;
    }
  }

  open(content) {
    this.modalService.open(content).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }
}
