import { Component, OnInit } from '@angular/core';
import 'fullcalendar';
import { weekdays } from 'moment';
import { PrestamoService } from '../../services/prestamo.service';
import { LibroService } from '../../services/libro.service';
import { UsuarioService } from '../../services/usuario.service';

@Component({
  selector: 'app-calendario-dos',
  templateUrl: './calendario.component.html',
  styleUrls: ['./calendario.component.css']
})
export class CalendarioComponent implements OnInit {

  idRol: number;
  events: any = [];
  lista = [];
  listaUsuarios = [];
  listaPrestamos = [];
  listaLibros = [];

  constructor(private usuarioService: UsuarioService,private libroService: LibroService,private prestamoService: PrestamoService) { }

  ngOnInit() {
    this.getUsuarios();
    this.idRol = parseInt(localStorage.getItem('idRol'));
    this.getLibros();
    if(this.idRol == 2){
      this.getPrestamosUsuario();
    }else if(this.idRol == 1){
      this.getPrestamosUsuarios();
    }
  }

  getCalendario(){
    $('#calendar').fullCalendar({
      defaultView: 'month',
      dayClick: function() {
      },
      eventLimit: true,
      validRange: {
        start: '2017-01-01',
        end: '2025-01-01'
      },
      events: this.events,
      height: 600,
      columnHeaderFormat: 'ddd'
    })
    var locale = $('#calendar').fullCalendar('option', 'locale','es');
  }

  //OBTENEMOS TODOS LOS PRESTAMOS REALIZADOS POR EL USUARIO
  getPrestamosUsuario(){
    this.prestamoService.getPrestamos().subscribe(res => {
      let eventss = [];
      console.log(res.json())
      this.lista = res.json();
      for(let i = 0; i < this.lista.length; i++){
        if(this.lista[i].idUsuario == parseInt(localStorage.getItem('idUsuario'))){
          this.listaPrestamos.push(this.lista[i]);
        }
      }
      for(const elemento of this.listaPrestamos){
        let datos: any = {
          title: '',
          start: '',
          color: '',
          textColor: 'white'
        };
        datos.title = "Entrega Libro: " + " " + this.obtenerTitulo(elemento.idLibro);
        datos.start = elemento.fechaFinal;
        datos.color = this.obtenerColorPorLibro(elemento.idLibro);
        eventss.push(datos);
        this.events = eventss;
      }
      this.getCalendario();
    })
  }

  
  //OBTENEMOS TODOS LOS PRESTAMOS REALIZADOS
  getPrestamosUsuarios(){
    this.prestamoService.getPrestamos().subscribe(res => {
      let eventss = [];
      console.log(res.json())
      this.lista = res.json();
      for(const elemento of this.lista){
        let datos: any = {
          title: '',
          start: '',
          color: '',
          textColor: 'white'
        };
        datos.title = this.obtenerNombre(elemento.idUsuario) + " Entrega Libro: " + " " + this.obtenerTitulo(elemento.idLibro);
        datos.start = elemento.fechaFinal;
        datos.color = this.obtenerColorPorUsuario(elemento.idUsuario);
        eventss.push(datos);
        this.events = eventss;
      }
      this.getCalendario();
    })
  }
  
  //ASIGNA EL COLOR SEGUN EL LIBRO
  obtenerColorPorLibro(idLibro: number){
    if(idLibro <= 5){
      return "#DF1919";
    }else if(idLibro > 5){
      return "#CC602E";
    }else if(idLibro > 10) {
      return "#2E77CC";
    } 
  }

  //ASIGNA EL COLOR SEGUN EL USUARIO
  obtenerColorPorUsuario(idUsuario: number){
    if(idUsuario == 1 || idUsuario == 5 ){
      return "#DF1919";
    }else if(idUsuario == 2 || idUsuario == 6){
      return "#CC602E";
    }else if(idUsuario == 3 || idUsuario == 7) {
      return "#2E77CC";
    } 
  }

  //OBTENEMOS TODOS LOS LIBROS
  getLibros(){
    this.libroService.getLibros().subscribe(res => {
      this.listaLibros = res.json();
    })
  }

  //OBTENEMOS TODOS LOS USUARIOS
  getUsuarios(){
    this.usuarioService.getUsuarios().subscribe(res => {
      this.listaUsuarios = res.json();
    })
  }  

  //OBTENEMOS EL NOMBRE DEL USUARIO SEGUN EL ID DEL USUARIO
  obtenerNombre(idUsuario: number){
    for(let i = 0; i < this.listaUsuarios.length; i++){
      if(this.listaUsuarios[i].idUsuario == idUsuario){
        return this.listaUsuarios[i].nombre + " "+ this.listaUsuarios[i].apellido;
      }
    }
  }  

  //OBTENEMOS EL TITULO SEGUN EL ID DEL LIBRO PRESTADO
  obtenerTitulo(idLibro: number){
    for(let i = 0; i < this.listaLibros.length; i++){
      if(this.listaLibros[i].idLibro == idLibro){
        return this.listaLibros[i].titulo;
      }
    }
  }
}
