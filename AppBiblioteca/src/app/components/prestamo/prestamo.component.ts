import { Component, OnInit } from '@angular/core';
import { PrestamoService } from '../../services/prestamo.service';
import { LibroService } from '../../services/libro.service';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';
import { UsuarioService } from '../../services/usuario.service';

@Component({
  selector: 'app-prestamo',
  templateUrl: './prestamo.component.html',
  styleUrls: ['./prestamo.component.css']
})
export class PrestamoComponent implements OnInit {

  idRol: number;
  closeResult: string;
  lista = [];
  listaUsuarios = [];
  listaLibros = [];
  listaFija = [];
  msgError = false;
  idPrestamo: number;
  idLibro: number;
  idUsuario: number;
  fechaInicio: Date;
  fechaFinal: Date;

  constructor(private prestamoService: PrestamoService,private libroService: LibroService, private usuarioService: UsuarioService , private modalService: NgbModal, private router: Router) { }

  ngOnInit() {
    this.idRol = parseInt(localStorage.getItem('idRol'));
    this.getLibros();
    this.getUsuarios();
    this.getPrestamos();
  }
  
  //OBTENER-PORTADA
  obtenerPortada(idLibro){
    for(let i = 0; i< this.listaLibros.length; i++){
      if(this.listaLibros[i].idLibro === idLibro){
        return this.listaLibros[i].portada;
      }
    }
  }

  //OBTENER-LIBRO
  obtenerLibro(idLibro){
    for(let i = 0; i< this.listaLibros.length; i++){
      if(this.listaLibros[i].idLibro === idLibro){
        return this.listaLibros[i].titulo;
      }
    }
  }

  //OBTENER-USUARIO
  obtenerUsuario(idUsuario){
    for(let i = 0; i< this.listaUsuarios.length; i++){
      if(this.listaUsuarios[i].idUsuario === idUsuario){
        return this.listaUsuarios[i].nombre +" "+ this.listaUsuarios[i].apellido;
      }
    }
  }

  //GET-PRESTAMOS
  getPrestamos() {
    this.prestamoService.getPrestamos().subscribe(res => {
      this.lista = res.json();
      this.listaFija = res.json();
    });
  }

  //GET-USUARIOS
  getUsuarios() {
    this.usuarioService.getUsuarios().subscribe(res => {
      this.listaUsuarios = res.json();
    });
  }

  //GET-LIBROS
  getLibros() {
    this.libroService.getLibros().subscribe(res => {
      this.listaLibros = res.json();
    });
  }

  //SUMAR DISPONIBLES AL DEVOLVER UN LIBRO
  sumarDisponibles(idLibro: number){
    const CANTIDAD = {
      idLibro: this.idLibro,
      cantidad: 1
    };
    this.prestamoService.sumarDisponibles(CANTIDAD).subscribe(response => {
      this.getPrestamos();
    });
  }

  //ELIMINAR PRESTAMO
  deletePrestamo() {
    this.prestamoService.deletePrestamo(this.idPrestamo).subscribe(
      (data) => {
        this.getPrestamos();
      }
    );
    this.sumarDisponibles(this.idLibro);
  }

  //ENVIAMOS LA DATA A LOS INPUTS
  setData(item: any) {
    this.idPrestamo = item.idPrestamo;
    this.idLibro = item.idLibro;
    this.idUsuario = item.idUsuario;
    this.fechaInicio = item.fechaInicio;
    this.fechaFinal = item.fechaFinal;
  }

  //BUSCADOR
  buscarPrestamo(text: any) {
    this.lista = this.listaFija;
    const list: any = [];
    if (text !== '') {
      for (const elemento of this.lista){
        if (elemento.idLibro.toLowerCase().indexOf(text.toLowerCase()) > -1 ||
          elemento.idUsuario.toLowerCase().indexOf(text.toLowerCase()) > -1) {
          list.push(elemento);
          this.lista = list;
          this.msgError = false;
        }else {
          if (list.length === 0) {
            this.lista = [];
            this.msgError = true;
          }
        }
      }
    }else {
      this.getPrestamos();
      this.msgError = false;
    }
}

open(content) {
    this.modalService.open(content).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
}

private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
}
}