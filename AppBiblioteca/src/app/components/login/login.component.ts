import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgForm } from '@angular/forms';
import { UsuarioService } from '../../services/usuario.service';
import { Jsonp } from '@angular/http';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  lista = [];
  usuario: string;
  pass: string;
  msgError = false;
  public token;
  public status: string;
  public estado: boolean;
  constructor(private router: Router, private usuarioService: UsuarioService, private activatedRouted: ActivatedRoute) {}

  ngOnInit() {
  }

  login(usuario: string, pass: string){
    this.usuarioService.login(this.usuario,this.pass, 'true').subscribe(
      response => {
        localStorage.setItem('idUsuario', JSON.stringify(response.json().idUsuario));
        localStorage.setItem('idRol' , JSON.stringify(response.json().idRol));
        localStorage.setItem('nombre', JSON.stringify(response.json().nombre));
        this.estado = response.json().estado;
        this.token = response.json().token;

        console.log(this.token);
        if(this.token <= 0){
          this.status = 'error';
        }else{
          //PERSISTIR DATOS DEL USUARIO
          localStorage.setItem('token', this.token);
        }
        if(this.estado){
          this.router.navigate(['/home']);
        }else{
          this.router.navigate(['/login']);
        }
      },
      error=>{
        var errorMessage = <any>error;
        console.log(errorMessage);
        if(errorMessage != null){
          this.status = 'error';
        }
      }
    );
  }
}
