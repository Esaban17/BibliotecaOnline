import { Component, OnInit } from '@angular/core';
import { UsuarioService } from '../../services/usuario.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  public token;
  usuario: string;
  nombre: string;

  constructor(private router: Router, private usuarioService: UsuarioService) {
    this.token = this.usuarioService.getToken();
  }

  ngDoCheck(){
    this.token = this.usuarioService.getToken();
    this.nombre = localStorage.getItem('nombre');
  }

  ngOnInit() {
    this.token = this.usuarioService.getToken();
  }

}
