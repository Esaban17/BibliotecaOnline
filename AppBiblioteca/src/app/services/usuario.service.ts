import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';

import 'rxjs/Observable';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class UsuarioService {

  public token;
  identity;
  private rutaImagen = 'http://localhost:3000/imagen/';
  private rutaUpload = 'http://localhost:3000/upload/foto/';
  private ruta = 'http://localhost:3000/usuarios/';
  private rutaLogin = 'http://localhost:3000/auth/';
  private rutaCUD = 'http://localhost:3000/usuario/';

  constructor(private http:Http, private router: Router) { }

  login(usuario: string, pass: string , token = null){
    if(token != null){
      this.token = token;
    }
    const BODY = {
      usuario: usuario,
      pass: pass
    }
    return this.http.post(this.rutaLogin, BODY);
  }

  subirArchivo(file){
    let formData = new FormData();
    formData.append('imagen', file);
    formData.append('idUsuario', localStorage.getItem('idUsuario'));
    return this.http.post(this.rutaUpload, formData);
  }

  getUsuarios() {
    return this.http.get(this.ruta);
  }

  getImagen(idUsuario: number){
    return this.http.get(this.rutaImagen + idUsuario)
  }

  getUsuario(idUsuario: number){
    return this.http.get(this.rutaCUD + idUsuario)
  }

  createUsuario(obj: any) {
    const BODY = obj;
    return this.http.post(this.rutaCUD, BODY);
  }

  updateUsuario(obj: any, idUsuario: number) {
    const BODY = obj;
    return this.http.put(this.rutaCUD + idUsuario, BODY);
  }

  deleteUsuario(idUsuario: number) {
    return this.http.delete(this.rutaCUD + idUsuario);
  }
  
  getToken(){
    let token = localStorage.getItem('token');
    if(token != "undefined"){
        this.token = token;
    }else{
        this.token = null;
    }
    return this.token;
  }
}
