import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';

import 'rxjs/Observable';

@Injectable()
export class PrestamoService {

  private ruta = 'http://localhost:3000/prestamos/';
  private rutaCUD = 'http://localhost:3000/prestamo/';
  private rutaRestarDisponibles = 'http://localhost:3000/prestamo/restar/disponibles';
  private rutaSumarDisponibles = 'http://localhost:3000/prestamo/sumar/disponibles';

  constructor(private http: Http) { }

  getPrestamos() {
    return this.http.get(this.ruta);
  }

  restarDisponibles(obj: any) {
    const BODY = obj;
    console.log(BODY);
    return this.http.put(this.rutaRestarDisponibles, BODY);
  }

  sumarDisponibles(obj: any) {
    const BODY = obj;
    console.log(BODY);
    return this.http.put(this.rutaSumarDisponibles, BODY);
  }

  createPrestamo(obj: any) {
    const BODY = obj;
    return this.http.post(this.rutaCUD, BODY);
  }

  updatePrestamo(idPrestamo: number, obj: any) {
    const BODY = obj;
    return this.http.put(this.rutaCUD + idPrestamo, BODY);
  }

  deletePrestamo(idPrestamo: number) {
    return this.http.delete(this.rutaCUD + idPrestamo);
  }
}

