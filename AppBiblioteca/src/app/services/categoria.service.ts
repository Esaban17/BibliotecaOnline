import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';

import 'rxjs/Observable';

@Injectable()
export class CategoriaService {

  private ruta = 'http://localhost:3000/categorias/';
  private rutaCUD = 'http://localhost:3000/categoria/';

  constructor(private http: Http) { }

  getCategorias() {
    return this.http.get(this.ruta);
  }

  createCategoria(obj: any) {
    const BODY = obj;
    return this.http.post(this.rutaCUD, BODY);
  }

  updateCategoria(idCategoria: number,obj: any) {
    const BODY = obj;
    return this.http.put(this.rutaCUD + idCategoria, BODY);
  }

  deleteCategoria(idCategoria: number) {
    return this.http.delete(this.rutaCUD + idCategoria);
  }
}
