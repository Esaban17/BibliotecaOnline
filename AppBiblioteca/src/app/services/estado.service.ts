import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';

import 'rxjs/Observable';

@Injectable()
export class EstadoService {

  private ruta = 'http://localhost:3000/estados/';
  private rutaCUD = 'http://localhost:3000/estado/';

  constructor(private http: Http) { }

  getEstados() {
    return this.http.get(this.ruta);
  }

  createEstado(obj: any) {
    const BODY = obj;
    return this.http.post(this.rutaCUD, BODY);
  }

  updateEstado(obj: any, idEstado: number) {
    const BODY = obj;
    return this.http.put(this.rutaCUD + idEstado, BODY);
  }

  deleteEstado(idEstado: number) {
    return this.http.delete(this.rutaCUD + idEstado);
  }
}