import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';

import 'rxjs/Observable';

@Injectable()
export class LibroService {

  private rutaUpload = 'http://localhost:3000/upload/portada';
  private ruta = 'http://localhost:3000/libros/';
  private rutaCUD = 'http://localhost:3000/libro/';
  private rutaDisponibles= 'http://localhost:3000/libro/disponibles';
  private rutaEstado = 'http://localhost:3000/libro/estado';

  constructor(private http: Http) { }

  getLibros() {
    return this.http.get(this.ruta);
  }

  subirPortada(idLibro,file){
    let formData = new FormData();
    formData.append('portada', file);
    formData.append('idLibro', idLibro);
    return this.http.post(this.rutaUpload, formData);
  }

  createLibro(obj: any) {
    const BODY = obj;
    return this.http.post(this.rutaCUD, BODY);
  }

  updateEstado(idLibro: number) {
    const BODY = idLibro;
    return this.http.put(this.rutaEstado, BODY);
  }

  updateLibro(idLibro: number,obj: any) {
    const BODY = obj;
    return this.http.put(this.rutaCUD + idLibro, BODY);
  }

  modificarDisponibles(obj: any) {
    const BODY = obj;
    console.log(BODY);
    return this.http.put(this.rutaDisponibles, BODY);
  }

  deleteLibro(idLibro: number) {
    return this.http.delete(this.rutaCUD + idLibro);
  }
}
