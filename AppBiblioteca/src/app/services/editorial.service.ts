import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';

import 'rxjs/Observable';

@Injectable()
export class EditorialService {

  private ruta = 'http://localhost:3000/editoriales/';
  private rutaCUD = 'http://localhost:3000/editorial/';

  constructor(private http: Http) { }

  getEditoriales() {
    return this.http.get(this.ruta);
  }

  createEditorial(obj: any) {
    const BODY = obj;
    return this.http.post(this.rutaCUD, BODY);
  }

  updateEditorial(idEditorial: number,obj: any) {
    const BODY = obj;
    return this.http.put(this.rutaCUD + idEditorial, BODY);
  }

  deleteEditorial(idEditorial: number) {
    return this.http.delete(this.rutaCUD + idEditorial);
  }
}
