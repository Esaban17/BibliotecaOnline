import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';

import 'rxjs/Observable';

@Injectable()
export class MensajeService {

  private ruta = 'http://localhost:3000/mensajes/';
  private rutaCUD = 'http://localhost:3000/mensaje/';
  private rutaCHAT = 'http://localhost:3000/chat/';

  constructor(private http: Http) { }

  getMensajes(idEmisor: number, idReceptor: number) {
    return this.http.get(this.rutaCHAT + idEmisor + '/' + idReceptor);
  }

  createMensaje(obj: any) {
    const BODY = obj;
    return this.http.post(this.rutaCUD, BODY);
  }

  updateMensaje(obj: any, idMensaje: number) {
    const BODY = obj;
    return this.http.put(this.rutaCUD + idMensaje, BODY);
  }

  deleteMensaje(idMensaje: number) {
    return this.http.delete(this.rutaCUD + idMensaje);
  }
}
