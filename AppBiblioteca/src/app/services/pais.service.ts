import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';

import 'rxjs/Observable';

@Injectable()
export class PaisService {

  private ruta = 'http://localhost:3000/paises/';
  private rutaCUD = 'http://localhost:3000/pais/';

  constructor(private http: Http) { }

  getPaises() {
    return this.http.get(this.ruta);
  }

  createPais(obj: any) {
    const BODY = obj;
    return this.http.post(this.rutaCUD, BODY);
  }

  updatePais(obj: any, idPais: number) {
    const BODY = obj;
    return this.http.put(this.rutaCUD + idPais, BODY);
  }

  deletePais(idPais: number) {
    return this.http.delete(this.rutaCUD + idPais);
  }
}
