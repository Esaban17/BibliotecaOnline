import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';

import 'rxjs/Observable';

@Injectable()
export class RolService {

  private ruta = 'http://localhost:3000/roles/';
  private rutaCUD = 'http://localhost:3000/rol/';

  constructor(private http: Http) { }

  getRoles() {
    return this.http.get(this.ruta);
  }

  createRol(obj: any) {
    const BODY = obj;
    return this.http.post(this.rutaCUD, BODY);
  }

  updateRol(obj: any, idRol: number) {
    const BODY = obj;
    return this.http.put(this.rutaCUD + idRol, BODY);
  }

  deleteRol(idRol: number) {
    return this.http.delete(this.rutaCUD + idRol);
  }
}
