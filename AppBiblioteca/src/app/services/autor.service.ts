import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';

import 'rxjs/Observable';

@Injectable()
export class AutorService {

  private rutaUpload = 'http://localhost:3000/upload/imagen';
  private ruta = 'http://localhost:3000/autores/';
  private rutaCUD = 'http://localhost:3000/autor/';

  constructor(private http: Http) { }

  getAutores() {
    return this.http.get(this.ruta);
  }

  subirFoto(idAutor,file){
    let formData = new FormData();
    formData.append('imagen', file);
    formData.append('idAutor', idAutor);
    return this.http.post(this.rutaUpload, formData);
  }

  createAutor(obj: any) {
    const BODY = obj;
    return this.http.post(this.rutaCUD, BODY);
  }

  updateAutor(idAutor: number,obj: any) {
    const BODY = obj;
    return this.http.put(this.rutaCUD + idAutor, BODY);
  }

  deleteAutor(idAutor: number) {
    return this.http.delete(this.rutaCUD + idAutor);
  }
}
