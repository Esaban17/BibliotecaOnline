import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';

import 'rxjs/Observable';

@Injectable()
export class ComentarioService {

  private ruta = 'http://localhost:3000/comentarios/';
  private rutaCUD = 'http://localhost:3000/comentario/';

  constructor(private http: Http) { }

  getComentarios() {
    return this.http.get(this.ruta);
  }

  createComentario(obj: any) {
    const BODY = obj;
    return this.http.post(this.rutaCUD, BODY);
  }

  updateComentario(obj: any, idComentario: number) {
    const BODY = obj;
    return this.http.put(this.rutaCUD + idComentario, BODY);
  }

  deleteComentario(idComentario: number) {
    return this.http.delete(this.rutaCUD + idComentario);
  }
}
