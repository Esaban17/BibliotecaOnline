USE virtualbookskinal;

/*... INSERT....*/

DELIMITER //
CREATE PROCEDURE insert_categoria
(IN categoria VARCHAR(128))
BEGIN
  INSERT INTO Categoria (categoria) VALUES(categoria);
END //
DELIMITER ;

CALL insert_categoria ('prueba')

/*... DELETE....*/
DELIMITER //
CREATE PROCEDURE edit_categoria
(IN p_idCategoria INT, p_categoria VARCHAR(128))
BEGIN
  UPDATE Categoria
  SET 
		categoria = p_categoria
  WHERE idCategoria = p_idCategoria;
END //
DELIMITER ;

CALL edit_categoria(1,'Poesía')

/*... DELETE....*/
DELIMITER //
CREATE PROCEDURE delete_categoria
(IN p_idCategoria INT)
BEGIN
	DELETE FROM Categoria
  WHERE idCategoria = p_idCategoria;
END //
DELIMITER ;

CALL delete_categoria(3);

SELECT * FROM Categoria;