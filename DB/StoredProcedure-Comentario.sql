USE virtualbookskinal;

/*... INSERT....*/

DELIMITER //
CREATE PROCEDURE insert_comentario
(IN idUsuario INT,
	idLibro INT,
    comentario VARCHAR(256)
)
BEGIN
  INSERT INTO Comentario (idUsuario,idLibro,comentario) 
		VALUES (idUsuario,idLibro,comentario);
END //
DELIMITER ;

CALL insert_comentario (2,4,'Me gustó mucho este libro')

/*... DELETE....*/
DELIMITER //
CREATE PROCEDURE edit_comentario
(IN p_idComentario INT,
	p_idUsuario INT,
	p_idLibro INT,
    p_comentario VARCHAR(256)
)
BEGIN
  UPDATE Comentario
  SET 
		idUsuario = p_idUsuario,
        idLibro = p_idLibro,
        comentario = p_comentario
  WHERE idComentario = p_idComentario;
END //
DELIMITER ;

CALL edit_comentario (1,1,1,'Me gustó mucho este libro')

/*... DELETE....*/
DELIMITER //
CREATE PROCEDURE delete_comentario
(IN p_idComentario INT)
BEGIN
	DELETE FROM Comentario
  WHERE idComentario = p_idComentario;
END //
DELIMITER ;

CALL delete_comentario(2);

SELECT * FROM Comentario;