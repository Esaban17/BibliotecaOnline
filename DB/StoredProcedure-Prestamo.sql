USE virtualbookskinal;

/*... INSERT....*/

DELIMITER //
CREATE PROCEDURE insert_prestamo
(IN p_idLibro INT,
	p_idUsuario INT,
    p_fechaInicio DATE,
    p_fechaFinal DATE,
    p_cantidad INT
)
BEGIN
  INSERT INTO Prestamo (idLibro,idUsuario,fechaInicio,fechaFinal,cantidad) 
		VALUES (p_idLibro,p_idUsuario,p_fechaInicio,p_fechaFinal,p_cantidad);
END //
DELIMITER ;

CALL insert_prestamo (4,2,'2018-02-15','2018-02-18',1)

/*RESTAR DISPONIBLES LIBROS*/
DELIMITER //
CREATE PROCEDURE restar_disponibles
(IN p_idLibro INT,
    p_cantidad INT)
BEGIN
  SET @restar = (SELECT disponibles FROM Libro WHERE idLibro = p_idLibro) - p_cantidad;
  UPDATE Libro
  SET
	disponibles = @restar
   WHERE idLibro = p_idLibro;
END //
DELIMITER ;

CALL restar_disponibles (1,1)

/*REGRESAR DISPONIBLES LIBROS*/
DELIMITER //
CREATE PROCEDURE sumar_disponibles
(IN p_idLibro INT,
    p_cantidad INT)
BEGIN
  SET @sumar = (SELECT disponibles FROM Libro WHERE idLibro = p_idLibro) + p_cantidad;
  UPDATE Libro
  SET
	disponibles = @sumar
   WHERE idLibro = p_idLibro;
END //
DELIMITER ;

CALL sumar_disponibles (1,1)

/*MODIFICAR CANTIDAD LIBROS*/
DELIMITER //
CREATE PROCEDURE modificar_cantidad
(IN p_idLibro INT,
  p_cantidad INT)
BEGIN
  UPDATE Libro
  SET
	cantidad = p_cantidad
   WHERE idLibro = p_idLibro;
END //
DELIMITER ;

CALL modificar_cantidad (1,10)


/*... DELETE....*/
DELIMITER //
CREATE PROCEDURE delete_prestamo
(IN p_idPrestamo INT)
BEGIN
	DELETE FROM Prestamo
  WHERE idPrestamo = p_idPrestamo;
END //
DELIMITER ;

CALL delete_prestamo(2);

SELECT * FROM Prestamo;