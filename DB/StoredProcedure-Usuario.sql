USE virtualbookskinal;

/*... AUTENTICAR....*/
DELIMITER //
CREATE PROCEDURE autenticar_usuario(IN _usuario VARCHAR(128), IN _pass VARCHAR(128))
	BEGIN
		SELECT * FROM Usuario WHERE
        usuario = _usuario AND pass = _pass;
    END //
DELIMITER ;

/*... CARGAR IMAGEN....*/
DELIMITER //
CREATE PROCEDURE cargar_imagen
(IN p_idUsuario INT, 
	p_foto VARCHAR(256)
)
BEGIN
  UPDATE Usuario
  SET 
		foto = p_foto
  WHERE idUsuario = p_idUsuario;
END //
DELIMITER ;

/*... INSERT....*/
DELIMITER //
CREATE PROCEDURE insert_usuario
(IN nombre VARCHAR(128),
	apellido VARCHAR(128),
    correo VARCHAR(128),
    telefono VARCHAR(9),
    usuario VARCHAR(128),
    pass VARCHAR(128),
    idRol INT
)
BEGIN
  INSERT INTO Usuario (nombre,apellido,correo,telefono,usuario,
						pass,idRol) 
		VALUES (nombre,apellido,correo,telefono,usuario,
						pass,idRol);
END //
DELIMITER ;

CALL insert_usuario ('Christian','Osorio','Cdosorio@gmail.com','5588-9596','Cosorio','123',1)

/*... DELETE....*/
DELIMITER //
CREATE PROCEDURE edit_usuario
(IN p_idUsuario INT, 
	p_nombre VARCHAR(128),
    p_apellido VARCHAR(128),
    p_correo VARCHAR(128),
    p_telefono VARCHAR(9),
    p_usuario VARCHAR(128),
    p_pass VARCHAR(128),
    p_idRol INT
)
BEGIN
  UPDATE Usuario
  SET 
		nombre = p_nombre,
        apellido = p_apellido,
        correo = p_correo,
        telefono = p_telefono,
        usuario = p_usuario,
        pass = p_pass,
        idRol = p_idRol
  WHERE idUsuario = p_idUsuario;
END //
DELIMITER ;

CALL edit_usuario(2,'Prueba','Prueba','8942-8564','pprueba','123',null,2)

/*... DELETE....*/
DELIMITER //
CREATE PROCEDURE delete_usuario
(IN p_idUsuario INT)
BEGIN
	DELETE FROM Usuario
  WHERE idUsuario = p_idUsuario;
END //
DELIMITER ;

CALL delete_usuario(4);

SELECT * FROM Usuario;