USE virtualbookskinal;

/*... INSERT....*/

DELIMITER //
CREATE PROCEDURE insert_estado
(IN estado VARCHAR(128))
BEGIN
  INSERT INTO Estado (estado) VALUES(estado);
END //
DELIMITER ;

CALL insert_estado ('Agotado')

/*... DELETE....*/
DELIMITER //
CREATE PROCEDURE edit_estado
(IN p_idEstado INT, p_estado VARCHAR(128))
BEGIN
  UPDATE Estado
  SET 
		estado = p_estado
  WHERE idEstado = p_idEstado;
END //
DELIMITER ;

CALL edit_estado(1,'Ocupado')

/*... DELETE....*/
DELIMITER //
CREATE PROCEDURE delete_estado
(IN p_idEstado INT)
BEGIN
	DELETE FROM Estado
  WHERE idEstado = p_idEstado;
END //
DELIMITER ;

CALL delete_estado(3);

SELECT * FROM Estado;