USE virtualbookskinal;

/*... INSERT....*/

DELIMITER //
CREATE PROCEDURE insert_autor
(IN nombre VARCHAR(128),
	apellido VARCHAR(128),
    biografia VARCHAR(1500),
    idPais INT
)
BEGIN
  INSERT INTO Autor (nombre,apellido,biografia,idPais) 
		VALUES(nombre,apellido,biografia,idPais);
END //
DELIMITER ;

CALL insert_autor ('Pablo','Neruda',1);


/*... CARGAR IMAGEN....*/
DELIMITER //
CREATE PROCEDURE cargar_foto
(IN p_idAutor INT, 
	p_foto VARCHAR(256)
)
BEGIN
  UPDATE Autor
  SET 
		foto = p_foto
  WHERE idAutor = p_idAutor;
END //
DELIMITER ;

/*... DELETE....*/
DELIMITER //
CREATE PROCEDURE edit_autor
(IN p_idAutor INT, 
	p_nombre VARCHAR(128),
    p_apellido VARCHAR(128),
    p_biografia VARCHAR(1500),
    p_idPais INT
)
BEGIN
  UPDATE Autor
  SET 
		nombre = p_nombre,
        apellido = p_apellido,
        idPais = p_idPais
  WHERE idAutor = p_idAutor;
END //
DELIMITER ;

CALL edit_autor(2,'Prueba','Prueba',4)

/*... DELETE....*/
DELIMITER //
CREATE PROCEDURE delete_autor
(IN p_idAutor INT)
BEGIN
	DELETE FROM Autor
  WHERE idAutor = p_idAutor;
END //
DELIMITER ;

CALL delete_autor(2);

select * from Autor
