USE virtualbookskinal;

/*... INSERT....*/
DELIMITER //
CREATE PROCEDURE insert_libro
(IN titulo VARCHAR(128),
	idAutor INT,
    idEditorial INT,
    edicion INT,
    tema VARCHAR(128),
    paginas INT,
	descripcion VARCHAR(256),
    idCategoria INT,
    cantidad INT,
    disponibles INT,
    fechaPublicacion VARCHAR(128),
    idEstado INT
)
BEGIN
  INSERT INTO Libro (titulo,idAutor,idEditorial,edicion,tema,paginas,
					descripcion,idCategoria,cantidad,disponibles,fechaPublicacion,idEstado) 
		VALUES (titulo,idAutor,idEditorial,edicion,tema,paginas,
					descripcion,idCategoria,cantidad,disponibles,fechaPublicacion,idEstado);
END //
DELIMITER ;

CALL insert_libro ('Cien años de Soledad',1,1,55,'Realismo',471,'Es una novela literaria',1,5,5,'1967')

/*... CARGAR PORTADA....*/
DELIMITER //
CREATE PROCEDURE cargar_portada
(IN p_idLibro INT, 
	p_portada VARCHAR(256)
)
BEGIN
  UPDATE Libro
  SET 
		portada = p_portada
  WHERE idLibro = p_idLibro;
END //
DELIMITER ;

/*... CARGAR PDF....*/
DELIMITER //
CREATE PROCEDURE cargar_pdf
(IN p_idLibro INT, 
	p_pdf VARCHAR(256)
)
BEGIN
  UPDATE Libro
  SET 
		pdf = p_pdf
  WHERE idLibro = p_idLibro;
END //
DELIMITER ;

/*... DELETE....*/
DELIMITER //
CREATE PROCEDURE edit_libro
(IN p_idLibro INT,
	p_titulo VARCHAR(128),
	p_idAutor INT,
    p_idEditorial INT,
    p_edicion INT,
    p_tema VARCHAR(128),
    p_paginas INT,
	p_descripcion VARCHAR(256),
    p_idCategoria INT,
    p_cantidad INT,
    p_disponibles INT,
    p_fechaPublicacion VARCHAR(128),
    p_idEstado INT
)
BEGIN
  UPDATE Libro
  SET 
		titulo = p_titulo,
        idAutor = p_idAutor,
        idEditorial = p_idEditorial,
        edicion = p_edicion,
        tema = p_tema,
        paginas = p_paginas,
		descripcion = p_descripcion,
		idCategoria = p_idCategoria,
		cantidad = p_cantidad,
		disponibles = p_disponibles,
		fechaPublicacion = p_fechaPublicacion,
        idEstado = p_idEstado
  WHERE idLibro = p_idLibro;
END //
DELIMITER ;

CALL edit_libro (1,'Cien años de soledad',1,1,55,'Realismo',471,'Es una novela literaria',1,5,5,1967)

/*... DELETE....*/
DELIMITER //
CREATE PROCEDURE delete_libro
(IN p_idLibro INT)
BEGIN
	DELETE FROM Libro
  WHERE idLibro = p_idLibro;
END //
DELIMITER ;

CALL delete_libro(2);

SELECT * FROM Libro;