CREATE DATABASE virtualbookskinal;

USE virtualbookskinal;

CREATE TABLE Rol (
	idRol INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    rol VARCHAR(128) NOT NULL
);

CREATE TABLE Usuario (
	idUsuario INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    nombre VARCHAR(128) NOT NULL,
    apellido VARCHAR(128) NOT NULL,
    correo VARCHAR(128) NOT NULL,
    telefono VARCHAR(9),
    usuario VARCHAR(128) NOT NULL UNIQUE,
    pass VARCHAR(128) NOT NULL,
    foto VARCHAR(256),
    idRol INT NOT NULL,
    FOREIGN KEY (idRol) REFERENCES Rol(idRol)
);

CREATE TABLE Pais (
	idPais INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    pais VARCHAR(128) NOT NULL
);

CREATE TABLE Autor (
	idAutor INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    nombre VARCHAR(128) NOT NULL,
    apellido VARCHAR(128) NOT NULL,
    biografia VARCHAR(1500) NOT NULL,
    foto VARCHAR(256),
    idPais INT NOT NULL,
    FOREIGN KEY (idPais) REFERENCES Pais(idPais)
);

CREATE TABLE Categoria (
	idCategoria INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    categoria VARCHAR(128) NOT NULL
);

CREATE TABLE Editorial (
	idEditorial INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    editorial VARCHAR(128) NOT NULL
);

CREATE TABLE Estado (
	idEstado INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    estado VARCHAR(128) NOT NULL
);

CREATE TABLE Libro (
	idLibro INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
	titulo VARCHAR(128) NOT NULL,
    idAutor INT NOT NULL,
    idEditorial INT NOT NULL,
    edicion INT NOT NULL,
    tema VARCHAR(128) NOT NULL,
    paginas INT NOT NULL,
    descripcion VARCHAR(256) NOT NULL,
    idCategoria INT NOT NULL,
    cantidad INT NOT NULL,
    disponibles INT NOT NULL,
    fechaPublicacion VARCHAR(128),
    portada VARCHAR(256),
    idEstado INT NOT NULL,
    FOREIGN KEY (idAutor) REFERENCES Autor(idAutor),
    FOREIGN KEY (idEditorial) REFERENCES Editorial(idEditorial),
    FOREIGN KEY (idCategoria) REFERENCES Categoria(idCategoria),
    FOREIGN KEY (idEstado) REFERENCES Estado(idEstado)
);

CREATE TABLE Comentario (
	idComentario INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    idUsuario INT NOT NULL,
    idLibro INT NOT NULL,
    comentario VARCHAR(256) NOT NULL,
	FOREIGN KEY (idUsuario) REFERENCES Usuario(idUsuario),
    FOREIGN KEY (idLibro) REFERENCES Libro(idLibro)
);

CREATE TABLE Prestamo (
	idPrestamo INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    idLibro INT NOT NULL,
    idUsuario INT NOT NULL,
    fechaInicio DATE NOT NULL,
    fechaFinal DATE NOT NULL,
    cantidad INT NOT NULL,
    FOREIGN KEY (idLibro) REFERENCES Libro(idLibro),
    FOREIGN KEY (idUsuario) REFERENCES Usuario(idUsuario)
);

CREATE TABLE Mensaje (
	idMensaje INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    idEmisor INT NOT NULL,
    idReceptor INT NOT NULL,
    mensaje VARCHAR(256) NOT NULL,
	FOREIGN KEY (idEmisor) REFERENCES Usuario(idUsuario),
    FOREIGN KEY (idReceptor) REFERENCES Usuario(idUsuario)
);


