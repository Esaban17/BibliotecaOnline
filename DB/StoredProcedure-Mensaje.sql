USE virtualbookskinal;

/*... INSERT....*/

DELIMITER //
CREATE PROCEDURE insert_mensaje
(IN idEmisor INT,
	idReceptor INT,
    mensaje VARCHAR(256)
)
BEGIN
  INSERT INTO Mensaje (idEmisor,idReceptor,mensaje) 
		VALUES (idEmisor,idReceptor,mensaje);
END //
DELIMITER ;

CALL insert_mensaje (2,2,'Me gustó mucho este libro')

/*... DELETE....*/
DELIMITER //
CREATE PROCEDURE edit_mensaje
(IN p_idMensaje INT,
	p_idEmisor INT,
	p_idReceptor INT,
    p_mensaje VARCHAR(256)
)
BEGIN
  UPDATE Mensaje
  SET 
		idEmisor = p_idEmisor,
        idReceptro = p_idReceptor,
        mensaje = p_mensaje
  WHERE idMensaje = p_idMensaje;
END //
DELIMITER ;

CALL edit_mensaje (1,1,1,'Me gustó mucho este libro')

/*... DELETE....*/
DELIMITER //
CREATE PROCEDURE delete_mensaje
(IN p_idMensaje INT)
BEGIN
	DELETE FROM Mensaje
  WHERE idMensaje = p_idMensaje;
END //
DELIMITER ;

CALL delete_mensaje(2);

SELECT * FROM Mensaje;