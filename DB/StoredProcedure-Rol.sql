USE virtualbookskinal;

/*... INSERT....*/

DELIMITER //
CREATE PROCEDURE insert_rol
(IN rol VARCHAR(128))
BEGIN
  INSERT INTO Rol (rol) VALUES(rol);
END //
DELIMITER ;

CALL insert_rol ('Usuario')

/*... DELETE....*/
DELIMITER //
CREATE PROCEDURE edit_rol
(IN p_idRol INT, p_rol VARCHAR(128))
BEGIN
  UPDATE Rol
  SET 
		rol = p_rol
  WHERE idRol = p_idRol;
END //
DELIMITER ;

CALL edit_rol(4,'prueba4')

/*... DELETE....*/
DELIMITER //
CREATE PROCEDURE delete_rol
(IN p_idRol INT)
BEGIN
	DELETE FROM Rol
  WHERE idRol = p_idRol;
END //
DELIMITER ;

CALL delete_rol(5);

SELECT * FROM Rol;