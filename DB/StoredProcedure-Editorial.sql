USE virtualbookskinal;

/*... INSERT....*/

DELIMITER //
CREATE PROCEDURE insert_editorial
(IN editorial VARCHAR(128))
BEGIN
  INSERT INTO Editorial (editorial) VALUES(editorial);
END //
DELIMITER ;

CALL insert_editorial ('prueba')

/*... DELETE....*/
DELIMITER //
CREATE PROCEDURE edit_editorial
(IN p_idEditorial INT, p_editorial VARCHAR(128))
BEGIN
  UPDATE Editorial
  SET 
		editorial = p_editorial
  WHERE idEditorial = p_idEditorial;
END //
DELIMITER ;

CALL edit_editorial(1,'Santillana')

/*... DELETE....*/
DELIMITER //
CREATE PROCEDURE delete_editorial
(IN p_idEditorial INT)
BEGIN
	DELETE FROM Editorial
  WHERE idEditorial = p_idEditorial;
END //
DELIMITER ;

CALL delete_editorial(2);

SELECT * FROM Editorial;