USE virtualbookskinal;

/*... INSERT....*/

DELIMITER //
CREATE PROCEDURE insert_pais
(IN pais VARCHAR(128))
BEGIN
  INSERT INTO Pais (pais) VALUES(pais);
END //
DELIMITER ;

CALL insert_pais ('Francia')

/*... DELETE....*/
DELIMITER //
CREATE PROCEDURE edit_pais
(IN p_idPais INT, p_pais VARCHAR(128))
BEGIN
  UPDATE Pais
  SET 
		pais = p_pais
  WHERE idPais = p_idPais;
END //
DELIMITER ;

CALL edit_pais(5,'Argentina')

/*... DELETE....*/
DELIMITER //
CREATE PROCEDURE delete_pais
(IN p_idPais INT)
BEGIN
	DELETE FROM Pais
  WHERE p_idPais = idPais;
END //
DELIMITER ;

CALL delete_pais(5); 

SELECT * FROM Pais;