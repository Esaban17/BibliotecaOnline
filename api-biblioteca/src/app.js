const express = require('express');
const cors = require('cors');
const app = express();

const morgan = require('morgan');
const bodyParser = require('body-parser');

//Settings
app.set('port', process.env.PORT || 3000);

//Middlewares
app.use(morgan('dev'));
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());
app.use(cors());

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE')
    next();
});

require('./routes/rolRoutes')(app);
require('./routes/paisRoutes')(app);
require('./routes/categoriaRoutes')(app);
require('./routes/autorRoutes')(app);
require('./routes/estadoRoutes')(app);
require('./routes/editorialRoutes')(app);
require('./routes/libroRoutes')(app);
require('./routes/comentarioRoutes')(app);
require('./routes/prestamoRoutes')(app);
require('./routes/usuarioRoutes')(app);
require('./routes/authRoutes')(app);
require('./routes/mensajeRoutes')(app);


app.listen(app.get('port'), () => {
    console.log(`Servidor Corriendo en el Puerto ${app.get('port')}`);
});