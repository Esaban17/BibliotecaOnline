const Comentario = require('../models/comentario');

module.exports = function(app){
    app.get('/comentarios', (req, res) => {
        Comentario.getComentarios((err, data) => {
            res.status(200).json(data);
        });
    });
    app.get('/comentario/:idComentario', (req, res) => {
        Comentario.getComentario(req.params.idComentario, (err, data) => {
            res.status(200).json(data);
        });
    });

    app.post('/comentario', (req, res) => {
        const comentarioData = {
            idComentario: null,
            idUsuario: req.body.idUsuario,
            idLibro: req.body.idLibro,
            comentario: req.body.comentario
        }

        Comentario.insertComentario(comentarioData, (err, data) => {
            if(data) {
                console.log(data);
                res.json({
                    success: true,
                    msg: 'Comentario Insertado Con Exito'
                });
            }else {
                res.status(500).json({
                    success: false,
                    msg: 'Error Al Insertar los Datos'
                });
            }
        });
    });
    
    app.put('/comentario/:idComentario', (req,  res) => {
        const comentarioData = {
            idComentario: req.params.idComentario,
            idUsuario: req.body.idUsuario,
            idLibro: req.body.idLibro,      
            comentario: req.body.comentario
        };
        Comentario.updateComentario(comentarioData, (err, data) =>{
            if(data && data.msg) {
                res.json(data);
            }else {
                res.json({
                    success: false,
                    msg: "Error"
                });
            }
        });
    });
    app.delete('/comentario/:idComentario', (req, res) => {
        Comentario.deleteComentario(req.params.idComentario, (err, data) => {
            if(data && data.msg === "deleted" || data.msg === "no existe") {
                res.json({
                    success: true,
                    data
                })
            }else {
                res.status(500).json({
                    msg: "Error"
                })
            }
        });
    });
}