const Usuario = require('../models/usuario');
const fse = require('fs-extra');
const formidable = require('formidable');

module.exports = function(app){
    app.get('/usuarios', (req, res) => {
        Usuario.getUsuarios((err, data) => {
            res.status(200).json(data);
        });
    });
    app.get('/usuario/:idUsuario', (req, res) => {
        Usuario.getUsuario(req.params.idUsuario, (err, data) => {
            res.status(200).json(data);
        });
    });

    app.get('/usuario/imagen/:foto', (req, res) => {
        var parametro = req.params.foto;
        var ruta = 'src/images/usuarios/';
        fse.readFile(ruta + parametro, (err, imagen) => {
            if(err){
                res.json({
                    message: 'Imagen No Encontrada'
                });
            }else{
                res.send(imagen);
            }
        })
    });

    app.post('/usuario', (req, res) => {
        const usuarioData = {
            idUsuario: null,
            nombre: req.body.nombre,
            apellido: req.body.apellido,
            correo: req.body.correo,
            telefono: req.body.telefono,
            usuario: req.body.usuario,
            pass: req.body.pass,
            idRol: req.body.idRol
        };

        Usuario.insertUsuario(usuarioData, (err, data) => {
            if(data) {
                console.log(data);
                res.json({
                    success: true,
                    msg: 'Usuario Insertado Con Exito'
                });
            }else {
                res.status(500).json({
                    success: false,
                    msg: 'Error Al Insertar los Datos'
                });
            }
        });
    });

    app.post('/upload/foto', (req, res, next) => {
        //Parseamos la Imagen
        var form = new formidable.IncomingForm();
        form.keepExtensions = true;
        form.maxFieldsSize = 10 * 1024 * 1024; //10 MB
        form.multiples = true;
        form.parse(req, (err, fields, files) => {
            if (err) {
                res.json({
                    result: "failed",
                    data: {},
                    messege: `No se pudo subir la imagen.Error is : ${err}`
                });
            }
            console.log(files)
            var arrayOfFiles = [];
            if(files[""] instanceof Array) {
                arrayOfFiles = files[""];
            } else {
                arrayOfFiles.push(files[""]);
            }         
            if (arrayOfFiles.length > 0) {
                //Cambiamos de ruta la imagen
                fse.copy(files.imagen.path, 'src/images/usuarios/'+ files.imagen.name, err => {
                    if (err) return console.error(err)
                    console.log('Exito al Subir la Imagen!')
                });
                const usuarioData = {
                    idUsuario: fields.idUsuario,
                    foto: files.imagen.name
                };
                Usuario.insertImagen(usuarioData, (err, data) => {
                    console.log(data)
                    if(data && data.msg) {
                        console.log('Imagen Insertada');
                    }else {
                        console.log('Error Al Insertar la Imagen')
                    }
                });
                res.json({
                    result: "Exito",
                    data: files.name,
                    messege: "Imagen Subida Correctamente",
                    files,
                    fields
                });
            } else {
                res.json({
                    result: "Error",
                    data: {},
                    numberOfImages: 0,
                    messege: "Imagen no Subida!"
                });
            }
        });
    });

    app.put('/usuario/:idUsuario', (req, res) => {
        const usuarioData = {
            idUsuario: req.params.idUsuario,
            nombre: req.body.nombre,
            apellido: req.body.apellido,
            correo: req.body.correo,
            telefono: req.body.telefono,
            usuario: req.body.usuario,
            pass: req.body.pass,
            idRol: req.body.idRol
        };
        Usuario.updateUsuario(usuarioData, (err, data) =>{
            if(data && data.msg) {
                res.json(data);
            }else {
                res.json({
                    success: false,
                    msg: "Error"
                });
            }
        });
    });
    app.delete('/usuario/:idUsuario', (req, res) => {
        Usuario.deleteUsuario(req.params.idUsuario, (err, data) => {
            if(data && data.msg === "deleted" || data.msg === "no existe") {
                res.json({
                    success: true,
                    data
                })
            }else {
                res.status(500).json({
                    msg: "Error"
                })
            }
        });
    });
}