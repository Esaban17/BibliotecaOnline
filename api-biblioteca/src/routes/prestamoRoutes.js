const Prestamo = require('../models/prestamo');

module.exports = function(app){
    app.get('/prestamos', (req, res) => {
        Prestamo.getPrestamos((err, data) => {
            res.status(200).json(data);
        });
    });
    app.get('/prestamo/:idPrestamo', (req, res) => {
        Prestamo.getPrestamo(req.params.idPrestamo, (err, data) => {
            res.status(200).json(data);
        });
    });

    app.post('/prestamo', (req, res) => {
        const prestamoData = {
            idPrestamo: null,
            idLibro: req.body.idLibro,
            idUsuario: req.body.idUsuario,
            fechaInicio: req.body.fechaInicio,
            fechaFinal: req.body.fechaFinal,
            cantidad: 1
        }

        Prestamo.insertPrestamo(prestamoData, (err, data) => {
            if(data) {
                console.log(data);
                res.json({
                    success: true,
                    msg: 'Prestamo Insertado Con Exito'
                });
            }else {
                res.status(500).json({
                    success: false,
                    msg: 'Error Al Insertar los Datos'
                });
            }
        });
    });

    app.put('/prestamo/restar/disponibles', (req,  res) => {
        const libroData = {
            idLibro: req.body.idLibro,
            cantidad: 1
        };
        console.log(libroData);
        Prestamo.restarDisponibles(libroData, (err, data) =>{
            if(data && data.msg) {
                res.json(data);
            }else {
                res.json({
                    success: false,
                    msg: "Error"
                });
            }
        });
    });

    app.put('/prestamo/sumar/disponibles', (req,  res) => {
        const libroData = {
            idLibro: req.body.idLibro,
            cantidad: 1
        };
        console.log(libroData);
        Prestamo.sumarDisponibles(libroData, (err, data) =>{
            if(data && data.msg) {
                res.json(data);
            }else {
                res.json({
                    success: false,
                    msg: "Error"
                });
            }
        });
    });

    app.delete('/prestamo/:idPrestamo', (req, res) => {
        Prestamo.deletePrestamo(req.params.idPrestamo, (err, data) => {
            if(data && data.msg === "deleted" || data.msg === "no existe") {
                res.json({
                    success: true,
                    data
                })
            }else {
                res.status(500).json({
                    msg: "Error"
                })
            }
        });
    });


}