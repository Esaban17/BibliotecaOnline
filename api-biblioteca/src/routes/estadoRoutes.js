const Estado = require('../models/estado');

module.exports = function(app) {
    app.get('/estados', (req, res) => {
        Estado.getEstados((err, data) => {
            res.status(200).json(data);
        });
    });
    
    app.get('/estado/:idEstado', (req, res) => {
        Estado.getEstado(req.params.idEstado, (err, data) => {
            res.status(200).json(data);
        });
    });
}