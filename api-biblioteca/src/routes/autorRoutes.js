const Autor = require('../models/autor');
const fse = require('fs-extra');
const formidable = require('formidable');

module.exports = function(app){
    app.get('/autores', (req, res) => {
        Autor.getAutores((err, data) => {
            res.status(200).json(data);
        });
    });
    app.get('/autor/:idAutor', (req, res) => {
        Autor.getAutor(req.params.idAutor, (err, data) => {
            res.status(200).json(data);
        });
    });

    app.get('/autor/imagen/:foto', (req, res) => {
        var parametro = req.params.foto;
        var ruta = 'src/images/autores/';
        fse.readFile(ruta + parametro, (err, imagen) => {
            if(err){
                res.json({
                    message: 'Imagen No Encontrada'
                });
            }else{
                res.send(imagen);
            }
        })
    });

    app.post('/autor', (req, res) => {
        const autorData = {
            idAutor: null,
            nombre: req.body.nombre,
            apellido: req.body.apellido,
            biografia: req.body.biografia,
            idPais: req.body.idPais
        }

        Autor.insertAutor(autorData, (err, data) => {
            if(data) {
                console.log(data);
                res.json({
                    success: true,
                    msg: 'Autor Insertado Con Exito'
                });
            }else {
                res.status(500).json({
                    success: false,
                    msg: 'Error Al Insertar los Datos'
                });
            }
        });
    });

    app.post('/upload/imagen', (req, res, next) => {
        //Parseamos la Imagen
        var form = new formidable.IncomingForm();
        form.keepExtensions = true;
        form.maxFieldsSize = 10 * 1024 * 1024; //10 MB
        form.multiples = true;
        form.parse(req, (err, fields, files) => {
            if (err) {
                res.json({
                    result: "failed",
                    data: {},
                    messege: `No se pudo subir la imagen.Error is : ${err}`
                });
            }
            console.log(files)
            var arrayOfFiles = [];
            if(files[""] instanceof Array) {
                arrayOfFiles = files[""];
            } else {
                arrayOfFiles.push(files[""]);
            }         
            if (arrayOfFiles.length > 0) {
                //Cambiamos de ruta la imagen
                fse.copy(files.imagen.path, 'src/images/autores/'+ files.imagen.name, err => {
                    if (err) return console.error(err)
                    console.log('Exito al Subir la Imagen!')
                });
                const autorData = {
                    idAutor: fields.idAutor,
                    foto: files.imagen.name
                };
                Autor.insertImagen(autorData, (err, data) => {
                    console.log(data)
                    if(data && data.msg) {
                        console.log('Imagen Insertada');
                    }else {
                        console.log('Error Al Insertar la Imagen')
                    }
                });
                res.json({
                    result: "Exito",
                    data: files.name,
                    messege: "Imagen Subida Correctamente",
                    files,
                    fields
                });
            } else {
                res.json({
                    result: "Error",
                    data: {},
                    numberOfImages: 0,
                    messege: "Imagen no Subida!"
                });
            }
        });
    });
    
    app.put('/autor/:idAutor', (req,  res) => {
        const autorData = {
            idAutor: req.params.idAutor,
            nombre: req.body.nombre,
            apellido: req.body.apellido,
            biografia: req.body.biografia,
            idPais: req.body.idPais
        };
        Autor.updateAutor(autorData, (err, data) =>{
            if(data && data.msg) {
                res.json(data);
            }else {
                res.json({
                    success: false,
                    msg: "Error"
                });
            }
        });
    });
    app.delete('/autor/:idAutor', (req, res) => {
        Autor.deleteAutor(req.params.idAutor, (err, data) => {
            if(data && data.msg === "deleted" || data.msg === "no existe") {
                res.json({
                    success: true,
                    data
                })
            }else {
                res.status(500).json({
                    msg: "Error"
                })
            }
        });
    });


}