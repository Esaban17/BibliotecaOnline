const Editorial = require('../models/editorial');

module.exports = function(app) {
    app.get('/editoriales', (req, res) => {
        Editorial.getEditoriales((err, data) => {
            res.status(200).json(data);
        });
    });
    app.get('/editorial/:idEditorial', (req, res) => {
        Editorial.getEditorial(req.params.idEditorial, (err, data) => {
            res.status(200).json(data);
        });
    });
    app.post('/editorial', (req, res) => {
        const editorialData = {
            idEditorial: null,
            editorial: req.body.editorial
        };

        Editorial.insertEditorial(editorialData, (err, data) => {
            if(data) {
                console.log(data);
                res.json({
                    success: true,
                    msg: 'Editorial Insertado Con Exito'
                });
            }else {
                res.status(500).json({
                    success: false,
                    msg: 'Error Al Insertar los Datos'
                });
            }
        });
    });
    app.put('/editorial/:idEditorial', (req,  res) => {
        const editorialData = {
            idEditorial: req.params.idEditorial,
            editorial: req.body.editorial
        };
        Editorial.updateEditorial(editorialData, (err, data) =>{
            if(data && data.msg) {
                res.json(data);
            }else {
                res.json({
                    success: false,
                    msg: "Error"
                });
            }
        });
    });
    app.delete('/editorial/:idEditorial', (req, res) => {
        Editorial.deleteEditorial(req.params.idEditorial, (err, data) => {
            if(data && data.msg === "deleted" || data.msg === "no existe") {
                res.json({
                    success: true,
                    data
                });
            }else {
                res.status(500).json({
                    msg: "Error"
                });
            }
        });
    });
}
