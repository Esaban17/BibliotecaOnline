const Mensaje = require('../models/mensaje');

module.exports = function(app){
    app.get('/mensajes', (req, res) => {
        Mensaje.getMensajes((err, data) => {
            res.status(200).json(data);
        });
    });
    app.get('/mensaje/:idMensaje', (req, res) => {
        Mensaje.getMensaje(req.params.idMensaje, (err, data) => {
            res.status(200).json(data);
        });
    });

    app.get('/chat/:idEmisor/:idReceptor', (req, res) => {
        const mensajeData = {
            idEmisor: req.params.idEmisor,
            idReceptor: req.params.idReceptor,
        }
        Mensaje.getMensajesChat(mensajeData, (err, data) => {
            res.status(200).json(data);
        });
    });

    app.post('/mensaje', (req, res) => {
        const mensajeData = {
            idMensaje: null,
            idEmisor: req.body.idEmisor,
            idReceptor: req.body.idReceptor,
            mensaje: req.body.mensaje
        }

        Mensaje.insertMensaje(mensajeData, (err, data) => {
            if(data) {
                console.log(data);
                res.json({
                    success: true,
                    msg: 'Mensaje Insertado Con Exito'
                });
            }else {
                res.status(500).json({
                    success: false,
                    msg: 'Error Al Insertar los Datos'
                });
            }
        });
    });
    
    app.put('/mensaje/:idMensaje', (req,  res) => {
        const mensajeData = {
            idMensaje: req.params.idMensaje,
            idEmisor: req.body.idEmisor,
            idReceptor: req.body.idReceptor,      
            mensaje: req.body.mensaje
        };
        Mensaje.updateMensaje(mensajeData, (err, data) =>{
            if(data && data.msg) {
                res.json(data);
            }else {
                res.json({
                    success: false,
                    msg: "Error"
                });
            }
        });
    });
    app.delete('/mensaje/:idMensaje', (req, res) => {
        Mensaje.delete(req.params.idMensaje, (err, data) => {
            if(data && data.msg === "deleted" || data.msg === "no existe") {
                res.json({
                    success: true,
                    data
                })
            }else {
                res.status(500).json({
                    msg: "Error"
                })
            }
        });
    });


}