const Pais = require('../models/pais');

module.exports = function(app) {
    app.get('/paises', (req, res) => {
        Pais.getPaises((err, data) => {
            res.status(200).json(data);
        });
    });
    
    app.get('/pais/:idPais', (req, res) => {
        Pais.getPais(req.params.idPais, (err, data) => {
            res.status(200).json(data);
        });
    });
}