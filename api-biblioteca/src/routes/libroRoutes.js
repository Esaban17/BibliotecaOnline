const Libro = require('../models/libro');
const fse = require('fs-extra');
const formidable = require('formidable');

module.exports = function(app){
    app.get('/libros', (req, res) => {
        Libro.getLibros((err, data) => {
            res.status(200).json(data);
        });
    });
    app.get('/libro/:idLibro', (req, res) => {
        Libro.getLibro(req.params.idLibro, (err, data) => {
            res.status(200).json(data);
        });
    });
    app.get('/libro/imagen/:portada', (req, res) => {
        var parametro = req.params.portada;
        var ruta = 'src/images/libros/';
        fse.readFile(ruta + parametro, (err, imagen) => {
            if(err){
                res.json({
                    message: 'Portada No Encontrada'
                });
            }else{
                res.send(imagen);
            }
        })
    });
    app.get('/libro/documento/:pdf', (req, res) => {
        var parametro = req.params.pdf;
        var ruta = 'src/docs/';
        fse.readFile(ruta + parametro, (err, pdf) => {
            if(err){
                res.json({
                    message: 'Pdf No Encontrado'
                });
            }else{
                res.send(pdf);
            }
        })
    });    
    app.post('/libro', (req, res) => {
        console.log(req.body.idEstado);
        const libroData = {
            titulo: req.body.titulo,
            idAutor: req.body.idAutor,
            idEditorial: req.body.idEditorial,
            edicion: req.body.edicion,
            tema: req.body.tema,
            paginas: req.body.paginas,
            descripcion: req.body.descripcion,
            idCategoria: req.body.idCategoria,
            cantidad: req.body.cantidad,
            disponibles: req.body.disponibles,
            fechaPublicacion: req.body.fechaPublicacion,
            idEstado: req.body.idEstado
        }
        
        Libro.insertLibro(libroData, (err, data) => {
            if(data) {
                console.log(data);
                res.json({
                    success: true,
                    msg: 'Libro Insertado Con Exito'
                });
            }else {
                res.status(500).json({
                    success: false,
                    msg: 'Error Al Insertar los Datos'
                });
            }
        });
    });
    app.post('/upload/documento', (req, res, next) => {
        //Parseamos el Documento
        var form = new formidable.IncomingForm();
        form.keepExtensions = true;
        form.maxFieldsSize = 10 * 1024 * 1024; //10 MB
        form.multiples = true;
        form.parse(req, (err, fields, files) => {
            if (err) {
                res.json({
                    result: "failed",
                    data: {},
                    message: `No se pudo subir el Documento : ${err}`
                });
            }
            console.log(files)
            var arrayOfFiles = [];
            if(files[""] instanceof Array) {
                arrayOfFiles = files[""];
            } else {
                arrayOfFiles.push(files[""]);
            }         
            if (arrayOfFiles.length > 0) {
                //Cambiamos de ruta del Documento
                fse.copy(files.pdf.path, 'src/docs/'+ files.pdf.name, err => {
                    if (err) return console.error(err)
                    console.log('Exito al Subir la Portada!')
                });
                const libroData = {
                    idLibro: fields.idLibro,
                    pdf: files.pdf.name
                };
                Libro.insertPdf(libroData, (err, data) => {
                    console.log(data)
                    if(data && data.msg) {
                        console.log('Pdf Insertado');
                    }else {
                        console.log('Error Al Insertar el Pdf')
                    }
                });
                res.json({
                    result: "Exito",
                    data: files.name,
                    message: "Pdf Subido Correctamente",
                    files,
                    fields
                });
            }else {
                res.json({
                    result: "Error",
                    data: {},
                    message: "Pdf no Subido!"
                });
            }
        });
    });    
    app.post('/upload/portada', (req, res, next) => {
        //Parseamos la Imagen
        var form = new formidable.IncomingForm();
        form.keepExtensions = true;
        form.maxFieldsSize = 10 * 1024 * 1024; //10 MB
        form.multiples = true;
        form.parse(req, (err, fields, files) => {
            if (err) {
                res.json({
                    result: "failed",
                    data: {},
                    message: `No se pudo subir la imagen.Error is : ${err}`
                });
            }
            console.log(files)
            var arrayOfFiles = [];
            if(files[""] instanceof Array) {
                arrayOfFiles = files[""];
            } else {
                arrayOfFiles.push(files[""]);
            }         
            if (arrayOfFiles.length > 0) {
                //Cambiamos de ruta la imagen
                fse.copy(files.portada.path, 'src/images/libros/'+ files.portada.name, err => {
                    if (err) return console.error(err)
                    console.log('Exito al Subir la Portada!')
                });
                const libroData = {
                    idLibro: fields.idLibro,
                    portada: files.portada.name
                };
                Libro.insertPortada(libroData, (err, data) => {
                    console.log(data)
                    if(data && data.msg) {
                        console.log('Portada Insertada');
                    }else {
                        console.log('Error Al Insertar la Portada')
                    }
                });
                res.json({
                    result: "Exito",
                    data: files.name,
                    message: "Portada Subida Correctamente",
                    files,
                    fields
                });
            } else {
                res.json({
                    result: "Error",
                    data: {},
                    message: "Portada no Subida!"
                });
            }
        });
    });
    app.put('/libro/:idLibro', (req,  res) => {
        const libroData = {
            idLibro: req.params.idLibro,
            titulo: req.body.titulo,
            idAutor: req.body.idAutor,
            idEditorial: req.body.idEditorial,
            edicion: req.body.edicion,
            tema: req.body.tema,
            paginas: req.body.paginas,
            descripcion: req.body.descripcion,
            idCategoria: req.body.idCategoria,
            cantidad: req.body.cantidad,
            disponibles: req.body.disponibles,
            fechaPublicacion: req.body.fechaPublicacion,
            idEstado: req.body.idEstado
        };
        Libro.updateLibro(libroData, (err, data) =>{
            if(data && data.msg) {
                res.json(data);
            }else {
                res.json({
                    success: false,
                    msg: "Error"
                });
            }
        });
    });
    app.put('/libro/disponibles', (req,  res) => {
        const libroData = {
            idLibro: req.body.idLibro,
            disponibles: req.body.disponibles
        };
        Libro.updateDisponibles(libroData, (err, data) =>{
            if(data && data.msg) {
                res.json(data);
            }else {
                res.json({
                    success: false,
                    msg: "Error"
                });
            }
        });
    });
    app.put('/libro/estado', (req,  res) => {
        const libroData = {
            idLibro: req.body.idLibro,
            idEstado: 2
        };
        console.log(libroData);
        Libro.updateEstado(libroData, (err, data) =>{
            if(data && data.msg) {
                res.json(data);
            }else {
                res.json({
                    success: false,
                    msg: "Error"
                });
            }
        });
    });    
    app.delete('/libro/:idLibro', (req, res) => {
        Libro.deleteLibro(req.params.idLibro, (err, data) => {
            if(data && data.msg === "deleted" || data.msg === "no existe") {
                res.json({
                    success: true,
                    data
                })
            }else {
                res.status(500).json({
                    msg: "Error"
                })
            }
        });
    });
}
