const Rol = require('../models/rol');

module.exports = function(app) {
    app.get('/roles/', (req, res) => {
        Rol.getRoles((err, data) => {
            res.status(200).json(data);
        });
    });
    app.get('/rol/:idRol', (req, res) => {
        Rol.getRol(req.params.idRol, (err, data) => {
            res.status(200).json(data);
        });
    });
}