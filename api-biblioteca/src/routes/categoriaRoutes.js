const Categoria = require('../models/categoria');

module.exports = function(app){
    app.get('/categorias', (req, res) => {
        Categoria.getCategorias((err, data) => {
            res.status(200).json(data);
        });
    });
    app.get('/categoria/:idCategoria', (req, res) => {
        Categoria.getCategoria(req.params.idCategoria, (err, data) => {
            res.status(200).json(data);
        });
    });
    app.post('/categoria', (req, res) => {
        const categoriaData = {
            idCategoria: null,
            categoria: req.body.categoria
        }

        Categoria.insertCategoria(categoriaData, (err, data) => {
            if(data) {
                console.log(data);
                res.json({
                    success: true,
                    msg: 'Categoria Insertada Con Exito'
                });
            }else {
                res.status(500).json({
                    success: false,
                    msg: 'Error Al Insertar los Datos'
                });
            }
        });
    });
    app.put('/categoria/:idCategoria', (req,  res) => {
        const categoriaData = {
            idCategoria: req.params.idCategoria,
            categoria: req.body.categoria
        };
        Categoria.updateCategoria(categoriaData, (err, data) =>{
            if(data && data.msg) {
                res.json(data);
            }else {
                res.json({
                    success: false,
                    msg: "Error"
                });
            }
        });
    });
    app.delete('/categoria/:idCategoria', (req, res) => {
        Categoria.deleteCategoria(req.params.idCategoria, (err, data) => {
            if(data && data.msg === "deleted" || data.msg === "no existe") {
                res.json({
                    success: true,
                    data
                })
            }else {
                res.status(500).json({
                    msg: "Error"
                })
            }
        });
    });


}

