const connection = require('../db/conexionDB');

let autorModel = {};

autorModel.getAutores = (callback) => {
    if(connection) {
        connection.query('Select * from Autor',
        (err, rows) => {
            if(err) {
                throw err;
            }else {
                callback(null, rows);
            }
        }
        )
    }
};

autorModel.getAutor = (idAutor, callback) => {
    if(connection){
        connection.query('SELECT * FROM Autor WHERE idAutor = ?', idAutor,
        (err, rows) => {
            if(err) {
                throw err;
            }else {
                callback(null, rows)
            }
        }
    )
    }
};

autorModel.insertImagen = function(autorData, callback) {
    if(connection) {
        const sql = 'CALL cargar_foto(?, ?)';
            connection.query(sql, [autorData.idAutor, autorData.foto],(err, res) =>{
              if(err) {
                  throw err;
              }else {
                callback(null, {
                    "msg": "success"
                });
              }
          });
      }
};

autorModel.insertAutor = (autorData, callback) => {
    if(connection) {
        connection.query("CALL insert_autor(?,?,?,?)", [autorData.nombre, autorData.apellido, autorData.biografia, autorData.idPais],
        (err, res) => {
            if(err) {
                throw err;
            }else {
                callback(null, {'insertId': res.insertId})
            }
        }
    );
    }
};
autorModel.updateAutor = (autorData, callback) => {
    if(connection) {
        const sql = "CALL edit_autor(?,?,?,?,?)";

        connection.query(sql,
            [autorData.idAutor, autorData.nombre, autorData.apellido, autorData.biografia, autorData.idPais],
            (err, res) => {
            if(err) {
                throw err;
            }else {
                callback(null, {
                    "msg": "success"
                });
            }
        });
    }
};

autorModel.deleteAutor = (idAutor, callback) =>{
    if(connection) {
        const sql = `SELECT * FROM Autor WHERE idAutor = ${connection.escape(idAutor)}`; 
        
        connection.query(sql, (err, row) => {
            if(row) {
                let sql = `
                CALL delete_autor(${idAutor})`

                connection.query(sql, (err, res) => {
                    if(err) {
                        throw err;
                    }else {
                        callback(null, {
                            msg: "deleted"
                        });
                    }
                });
            }else {
                msg: "no existe"
            }
        });
    }
};

module.exports = autorModel;