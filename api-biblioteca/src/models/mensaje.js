const connection = require('../db/conexionDB');

let mensajeModel = {};

mensajeModel.getMensajes = (callback) => {
    if(connection) {
        connection.query('SELECT * FROM Mensaje',
        (err, rows) => {
            if(err) {
                throw err;
            }else {
                callback(null, rows);
            }
        }
        )
    }
};

mensajeModel.getMensaje = (idMensaje, callback) => {
    if(connection){
        connection.query('SELECT * FROM Mensaje WHERE idMensaje = ? ORDER BY idMensaje', idMensaje,
        (err, rows) => {
            if(err) {
                throw err;
            }else {
                callback(null, rows)
            }
        }
    )
    }
};

mensajeModel.getMensajesChat = (mensajeData, callback) => {
    if(connection){
        connection.query('SELECT * FROM Mensaje WHERE idEmisor = ? AND idReceptor = ? OR idReceptor = ? AND idEmisor = ? ORDER BY idMensaje', [mensajeData.idEmisor, mensajeData.idReceptor, mensajeData.idEmisor, mensajeData.idReceptor],
        (err, rows) => {
            if(err) {
                throw err;
            }else {
                callback(null, rows)
            }
        }
    )
    }
};

mensajeModel.insertMensaje = (mensajeData, callback) => {
    if(connection) {
        connection.query("CALL insert_mensaje(?,?,?)", [mensajeData.idEmisor, mensajeData.idReceptor, mensajeData.mensaje],
        (err, res) => {
            if(err) {
                throw err;
            }else {
                callback(null, {'insertId': res.insertId})
            }
        }
    );
    }
};
mensajeModel.updateMensaje = (mensajeData, callback) => {
    if(connection) {
        const sql = "CALL edit_mensaje(?,?,?,?)";

        connection.query(sql,
            [mensajeData.idMensaje, mensajeData.idEmisor, mensajeData.idReceptor, mensajeData.mensaje], 
            (err, res) => {
            if(err) {
                throw err;
            }else {
                callback(null, {
                    "msg": "success"
                });
            }
        });
    }
};

mensajeModel.deleteMensaje = (idMensaje, callback) =>{
    if(connection) {
        const sql = `
        CALL buscar_mensaje(${connection.escape(idMensaje)})`; 
        
        connection.query(sql, (err, row) => {
            if(row) {
                let sql = `
                CALL delete_mensaje(${idMensaje})`

                connection.query(sql, (err, res) => {
                    if(err) {
                        throw err;
                    }else {
                        callback(null, {
                            msg: "deleted"
                        });
                    }
                });
            }else {
                msg: "no existe"
            }
        });
    }
};

module.exports = mensajeModel;