const connection = require('../db/conexionDB');

let comentarioModel = {};

comentarioModel.getComentarios = (callback) => {
    if(connection) {
        connection.query('Select * from Comentario',
        (err, rows) => {
            if(err) {
                throw err;
            }else {
                callback(null, rows);
            }
        }
        )
    }
};

comentarioModel.getComentario = (idComentario, callback) => {
    if(connection){
        connection.query('SELECT * FROM Comentario WHERE idComentario = ?', idComentario,
        (err, rows) => {
            if(err) {
                throw err;
            }else {
                callback(null, rows)
            }
        }
    )
    }
};

comentarioModel.insertComentario = (comentarioData, callback) => {
    if(connection) {
        connection.query("CALL insert_comentario(?,?,?)", [comentarioData.idUsuario, comentarioData.idLibro, comentarioData.comentario],
        (err, res) => {
            if(err) {
                throw err;
            }else {
                callback(null, {'insertId': res.insertId})
            }
        }
    );
    }
};
comentarioModel.updateComentario = (comentarioData, callback) => {
    if(connection) {
        const sql = "CALL edit_comentario(?,?,?,?)";

        connection.query(sql,
            [comentarioData.idComentario, comentarioData.idUsuario, comentarioData.idLibro, comentarioData.comentario], 
            (err, res) => {
            if(err) {
                throw err;
            }else {
                callback(null, {
                    "msg": "success"
                });
            }
        });
    }
};

comentarioModel.deleteComentario = (idComentario, callback) =>{
    if(connection) {
        const sql = `
        CALL buscar_comentario(${connection.escape(idComentario)})`; 
        
        connection.query(sql, (err, row) => {
            if(row) {
                let sql = `
                CALL delete_comentario(${idComentario})`

                connection.query(sql, (err, res) => {
                    if(err) {
                        throw err;
                    }else {
                        callback(null, {
                            msg: "deleted"
                        });
                    }
                });
            }else {
                msg: "no existe"
            }
        });
    }
};

module.exports = comentarioModel;