const connection = require('../db/conexionDB');

let rolModel = {};

rolModel.getRoles = (callback) => {
    if(connection) {
        connection.query('SELECT * FROM Rol',
        (err, rows) => {
            if(err) {
                throw err;
            }else {
                callback(null, rows);
            }
        }
        )
    }
};

rolModel.getRol = (idRol, callback) => {
    if(connection){
        connection.query('SELECT * FROM Rol WHERE idRol = ?', idRol,
        (err, rows) => {
            if(err) {
                throw err;
            }else {
                callback(null, rows)
            }
        }
    )
    }
};

module.exports = rolModel;