const connection = require('../db/conexionDB');

let libroModel = {};

libroModel.getLibros = (callback) => {
    if(connection) {
        connection.query('SELECT * FROM Libro',
        (err, rows) => {
            if(err) {
                throw err;
            }else {
                callback(null, rows);
            }
        });
    }
};
libroModel.getLibro = (idLibro, callback) => {
    if(connection){
        connection.query('SELECT * FROM Libro WHERE idLibro = ?', idLibro,
        (err, rows) => {
            if(err) {
                throw err;
            }else {
                callback(null, rows)
            }
        }
        );
    }
};

libroModel.insertPortada = function(libroData, callback) {
    if(connection) {
        const sql = 'CALL cargar_portada(?, ?)';
            connection.query(sql, [libroData.idLibro, libroData.portada],(err, res) =>{
              if(err) {
                  throw err;
              }else {
                callback(null, {
                    "msg": "success"
                });
              }
          });
      }
};

libroModel.insertPdf = function(libroData, callback) {
    if(connection) {
        const sql = 'CALL cargar_pdf(?, ?)';
            connection.query(sql, [libroData.idLibro, libroData.pdf],(err, res) =>{
              if(err) {
                  throw err;
              }else {
                callback(null, {
                    "msg": "success"
                });
              }
          });
      }
};

libroModel.insertLibro = (libroData, callback) => {
    if(connection) {
        connection.query("CALL insert_libro(?,?,?,?,?,?,?,?,?,?,?,?)", [libroData.titulo, libroData.idAutor, libroData.idEditorial,
        libroData.edicion, libroData.tema, libroData.paginas, libroData.descripcion, libroData.idCategoria, libroData.cantidad,
        libroData.disponibles, libroData.fechaPublicacion, libroData.idEstado], 
        (err, res) => {
            if(err) {
                throw err;
            }else {
                callback(null, {'insertId': res.insertId})
            }
        }
    );
    }
};
libroModel.updateLibro = (libroData, callback) => {
    if(connection) {
        const sql = "CALL edit_libro(?,?,?,?,?,?,?,?,?,?,?,?,?)";
        connection.query(sql,
            [libroData.idLibro, libroData.titulo, libroData.idAutor, libroData.idEditorial, libroData.edicion, libroData.tema, libroData.paginas, libroData.descripcion,
            libroData.idCategoria, libroData.cantidad, libroData.disponibles, libroData.fechaPublicacion,libroData.idEstado], 
            (err, res) => {
            if(err) {
                throw err;
            }else {
                callback(null, {
                    "msg": "success"
                });
            }
        });
    }
};

libroModel.updateDisponibles = (libroData, callback) => {
    if(connection) {
        const sql = "CALL modificar_disponibles(?,?)";

        connection.query(sql,[libroData.idLibro, libroData.disponibles], 
            (err, res) => {
            if(err) {
                throw err;
            }else {
                callback(null, {
                    "msg": "success"
                });
            }
        });
    }
};

libroModel.updateEstado = (libroData, callback) => {
    if(connection) {
        const sql = "CALL edit_estadolibro(?,?)";

        connection.query(sql,[libroData.idLibro, libroData.idEstado], 
            (err, res) => {
            if(err) {
                throw err;
            }else {
                callback(null, {
                    "msg": "success"
                });
            }
        });
    }
};

libroModel.deleteLibro = (idLibro, callback) =>{
    if(connection) {
        const sql = `SELECT * FROM Libro WHERE idLibro = ${connection.escape(idLibro)}`; 
        
        connection.query(sql, (err, row) => {
            if(row) {
                let sql = `
                CALL delete_libro(${idLibro})`

                connection.query(sql, (err, res) => {
                    if(err) {
                        throw err;
                    }else {
                        callback(null, {
                            msg: "deleted"
                        });
                    }
                });
            }else {
                msg: "no existe"
            }
        });
    }
};

module.exports = libroModel;