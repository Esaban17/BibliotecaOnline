const connection = require('../db/conexionDB');

let categoriaModel = {};

categoriaModel.getCategorias = (callback) => {
    if(connection) {
        connection.query('Select * from Categoria',
        (err, rows) => {
            if(err) {
                throw err;
            }else {
                callback(null, rows);
            }
        }
        )
    }
};

categoriaModel.getCategoria = (idCategoria, callback) => {
    if(connection){
        connection.query('SELECT * FROM Categoria WHERE idCategoria = ?', idCategoria,
        (err, rows) => {
            if(err) {
                throw err;
            }else {
                callback(null, rows)
            }
        }
    )
    }
};

categoriaModel.insertCategoria = (categoriaData, callback) => {
    if(connection) {
        connection.query('CALL insert_categoria (?)', categoriaData.categoria,
        (err, res) => {
            if(err) {
                throw err;
            }else {
                callback(null, {'insertId': res.insertId})
            }
        }
    )
    }
};
categoriaModel.updateCategoria = (categoriaData, callback) => {
    if(connection) {
        const sql = "CALL edit_categoria(?,?)";

        connection.query(sql, [categoriaData.idCategoria, categoriaData.categoria],
            (err, res) => {
            if(err) {
                throw err;
            }else {
                callback(null, {
                    "msg": "success"
                });
            }
        });
    }
};

categoriaModel.deleteCategoria = (idCategoria, callback) =>{
    if(connection) {
        const sql = `
        CALL buscar_categoria(${connection.escape(idCategoria)})`; 
        
        connection.query(sql, (err, row) => {
            if(row) {
                let sql = `
                CALL delete_categoria(${idCategoria})`

                connection.query(sql, (err, res) => {
                    if(err) {
                        throw err;
                    }else {
                        callback(null, {
                            msg: "deleted"
                        });
                    }
                });
            }else {
                msg: "no existe"
            }
        });
    }
};

module.exports = categoriaModel;