const connection = require('../db/conexionDB');

let prestamoModel = {};

prestamoModel.getPrestamos = (callback) => {
    if(connection) {
        connection.query("SELECT idPrestamo, idUsuario, idLibro, DATE_FORMAT(fechaInicio, '%Y-%m-%d') AS fechaInicio, DATE_FORMAT(fechaFinal, '%Y-%m-%d') AS fechaFinal, cantidad FROM Prestamo",
        (err, rows) => {
            if(err) {
                throw err;
            }else {
                callback(null, rows);
            }
        }
        )
    }
};

prestamoModel.getPrestamo = (idPrestamo, callback) => {
    if(connection){
        connection.query('SELECT * FROM Prestamo WHERE idPrestamo = ?', idPrestamo,
        (err, rows) => {
            if(err) {
                throw err;
            }else {
                callback(null, rows)
            }
        }
    )
    }
};

prestamoModel.insertPrestamo = (prestamoData, callback) => {
    if(connection) {
        connection.query("CALL insert_prestamo(?,?,?,?,?)", [prestamoData.idLibro, prestamoData.idUsuario, prestamoData.fechaInicio,
        prestamoData.fechaFinal,prestamoData.cantidad],
        (err, res) => {
            if(err) {
                throw err;
            }else {
                callback(null, {'insertId': res.insertId})
            }
        }
    );
    }
};
prestamoModel.sumarDisponibles = (libroData, callback) => {
    if(connection) {
        const sql = "CALL sumar_disponibles(?,?)";

        connection.query(sql,[libroData.idLibro, libroData.cantidad], 
            (err, res) => {
            if(err) {
                throw err;
            }else {
                callback(null, {
                    "msg": "success"
                });
            }
        });
    }
};

prestamoModel.restarDisponibles = (libroData, callback) => {
    if(connection) {
        const sql = "CALL restar_disponibles(?,?)";

        connection.query(sql,[libroData.idLibro, libroData.cantidad], 
            (err, res) => {
            if(err) {
                throw err;
            }else {
                callback(null, {
                    "msg": "success"
                });
            }
        });
    }
};

prestamoModel.deletePrestamo = (idPrestamo, callback) =>{
    if(connection) {
        const sql = `SELECT * FROM PRESTAMO WHERE idPrestamo = ${connection.escape(idPrestamo)}`; 
        
        connection.query(sql, (err, row) => {
            if(row) {
                let sql = `CALL delete_prestamo(${idPrestamo})`

                connection.query(sql, (err, res) => {
                    if(err) {
                        throw err;
                    }else {
                        callback(null, {
                            msg: "deleted"
                        });
                    }
                });
            }else {
                msg: "no existe"
            }
        });
    }
};

module.exports = prestamoModel;