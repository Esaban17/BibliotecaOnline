const connection = require('../db/conexionDB');

let paisModel = {};

paisModel.getPaises = (callback) => {
    if(connection) {
        connection.query('Select * from Pais',
        (err, rows) => {
            if(err) {
                throw err;
            }else {
                callback(null, rows);
            }
        }
        )
    }
};
paisModel.getPais = (idPais, callback) => {
    if(connection){
        connection.query('SELECT * FROM Pais WHERE idPais = ?', idPais,
        (err, rows) => {
            if(err) {
                throw err;
            }else {
                callback(null, rows)
            }
        }
        )
    }
};

module.exports = paisModel;