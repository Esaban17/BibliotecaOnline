const connection = require('../db/conexionDB');

let editorialModel = {};

editorialModel.getEditoriales = (callback) => {
    if(connection) {
        connection.query('Select * from Editorial',
        (err, rows) => {
            if(err) {
                throw err;
            }else {
                callback(null, rows);
            }
        }
        )
    }
};
editorialModel.getEditorial = (idEditorial, callback) => {
    if(connection){
        connection.query('SELECT * FROM Editorial WHERE idEditorial = ?', idEditorial,
        (err, rows) => {
            if(err) {
                throw err;
            }else {
                callback(null, rows)
            }
        }
        )
    }
};
editorialModel.insertEditorial = (editorialData, callback) => {
    if(connection) {
        connection.query('CALL insert_editorial (?)', editorialData.editorial,
        (err, res) => {
            if(err) {
                throw err;
            }else {
                callback(null, {'insertId': res.insertId})
            }
        }
    )
    }
};
editorialModel.updateEditorial = (editorialData, callback) => {
    if(connection) {
        const sql = "CALL edit_editorial(?,?)";

        connection.query(sql, [editorialData.idEditorial, editorialData.editorial],
            (err, res) => {
            if(err) {
                throw err;
            }else {
                callback(null, {
                    "msg": "success"
                });
            }
        });
    }
};

editorialModel.deleteEditorial = (idEditorial, callback) =>{
    if(connection) {
        const sql = `
        CALL buscar_editorial(${connection.escape(idEditorial)})`; 
        
        connection.query(sql, (err, row) => {
            if(row) {
                let sql = `
                CALL delete_editorial(${idEditorial})`

                connection.query(sql, (err, res) => {
                    if(err) {
                        throw err;
                    }else {
                        callback(null, {
                            msg: "deleted"
                        });
                    }
                });
            }else {
                msg: "no existe"
            }
        });
    }
};

module.exports = editorialModel;