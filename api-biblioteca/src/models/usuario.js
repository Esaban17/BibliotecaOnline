const connection = require('../db/conexionDB');

let usuarioModel = {};

usuarioModel.login = function(usuarioData, callback) {
    if(connection) {
        const sql = 'CALL autenticar_usuario(?, ?)';
            connection.query(sql, [usuarioData.usuario, usuarioData.pass],(err, res) =>{
              if(err) {
                  throw err;
              } else {
                  callback(null, res[0]);
              }
          });
      }
};

usuarioModel.insertImagen = function(usuarioData, callback) {
    if(connection) {
        const sql = 'CALL cargar_imagen(?, ?)';
            connection.query(sql, [usuarioData.idUsuario, usuarioData.foto],(err, res) =>{
              if(err) {
                  throw err;
              }else {
                callback(null, {
                    "msg": "success"
                });
              }
          });
      }
};

usuarioModel.getUsuarios = (callback) => {
    if(connection) {
        connection.query('SELECT * FROM Usuario',
        (err, rows) => {
            if(err) {
                throw err;
            }else {
                callback(null, rows);
            }
        }
        )
    }
};

usuarioModel.getUsuario = (idUsuario, callback) => {
    if(connection){
        connection.query('SELECT * FROM Usuario WHERE idUsuario = ?', idUsuario,
        (err, rows) => {
            if(err) {
                throw err;
            }else {
                callback(null, rows)
            }
        }
    )
    }
};

usuarioModel.insertUsuario = (usuarioData, callback) => {
    if(connection) {
        connection.query("CALL insert_usuario(?,?,?,?,?,?,?)", [usuarioData.nombre, usuarioData.apellido, usuarioData.correo,
        usuarioData.telefono, usuarioData.usuario, usuarioData.pass, usuarioData.idRol],
        (err, res) => {
            if(err) {
                throw err;
            }else {
                callback(null, {'insertId': res.insertId})
            }
        }
    );
    }
};
usuarioModel.updateUsuario = (usuarioData, callback) => {
    if(connection) {
        const sql = "CALL edit_usuario(?,?,?,?,?,?,?,?)";

        connection.query(sql,
            [usuarioData.idUsuario, usuarioData.nombre, usuarioData.apellido, usuarioData.correo,
            usuarioData.telefono, usuarioData.usuario, usuarioData.pass, usuarioData.idRol], 
            (err, res) => {
            if(err) {
                throw err;
            }else {
                callback(null, {
                    "msg": "success"
                });
            }
        });
    }
};

usuarioModel.deleteUsuario = (idUsuario, callback) =>{
    if(connection) {
        const sql = `
        CALL buscar_usuario(${connection.escape(idUsuario)})`; 
        
        connection.query(sql, (err, row) => {
            if(row) {
                let sql = `
                CALL delete_usuario(${idUsuario})`

                connection.query(sql, (err, res) => {
                    if(err) {
                        throw err;
                    }else {
                        callback(null, {
                            msg: "deleted"
                        });
                    }
                });
            }else {
                msg: "no existe"
            }
        });
    }
};

module.exports = usuarioModel;