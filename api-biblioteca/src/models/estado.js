const connection = require('../db/conexionDB');

let estadoModel = {};

estadoModel.getEstados = (callback) => {
    if(connection) {
        connection.query('Select * from Estado',
        (err, rows) => {
            if(err) {
                throw err;
            }else {
                callback(null, rows);
            }
        }
        )
    }
};

estadoModel.getEstado = (idEstado, callback) => {
    if(connection){
        connection.query('SELECT * FROM Estado WHERE idEstado = ?', idEstado,
        (err, rows) => {
            if(err) {
                throw err;
            }else {
                callback(null, rows)
            }
        }
    )
    }
};

module.exports = estadoModel;